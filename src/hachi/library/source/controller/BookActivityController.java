package hachi.library.source.controller;

import hachi.library.source.activity.BookActivity;
import hachi.library.source.activity.LogInActivity;
import hachi.library.source.tab.LayoutBook;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

/**
 * Created by taohansamu on 5/10/2016.
 */
public class BookActivityController implements Initializable {
    @FXML
    private TextField txtBookname;
    @FXML
    private TextField txtBookprice;
    @FXML
    private TextField txtBookamount;
    @FXML
    private TextField txtAuthor;
    @FXML
    private Text actiontarget;
    @FXML
    private TextField txtBookRemaining;

    @FXML
    public void enterPressed(Event event) throws Exception {
        handleSubmitButtonAction();
    }

    @FXML
    public void handleSubmitButtonAction() throws Exception {
        if (txtAuthor.getText().equals("") || txtBookname.getText().equals("")
                || txtBookamount.getText().equals("") || txtBookprice.getText().equals("")
                || txtBookRemaining.getText().equals("")) {
            actiontarget.setText("Không được bỏ trống trường nào !");
        } else {
            BookActivity bookActivity=new BookActivity();
            bookActivity.updateBook(txtBookname.getText(),txtBookprice.getText(),txtBookamount.getText(),txtAuthor.getText(), txtBookRemaining.getText());
        }
    }

    @SuppressWarnings("unchecked")
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ResultSet rs = LogInActivity.pd.query("SELECT * FROM book WHERE book_id=" + BookActivity.bookId);
            if (rs.next()) {
                txtBookname.setText(rs.getString("book_name"));
                txtBookprice.setText(rs.getString("book_price"));
                txtBookamount.setText(rs.getString("book_amount"));
                txtBookRemaining.setText(rs.getString("book_storing"));
                txtAuthor.setText(rs.getString("author"));
            }
        } catch (Exception e) {

        }
    }

}