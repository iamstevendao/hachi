package hachi.library.source.activity;

import hachi.library.source.function.ProcessDatabase;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.ResultSet;

public class LogInActivity extends Application {

    public static ProcessDatabase pd;
    private static LogInActivity logInActivity;
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../../layout/log_in_activity.fxml"));
        logInActivity = this;
        Scene scene = new Scene(root, 300, 275);
        scene.getStylesheets().setAll(getClass().getResource("../../css/layout.css").toExternalForm());
        primaryStage.setTitle("Đăng Nhập");
        primaryStage.setScene(scene);
        primaryStage.show();
        pd = ProcessDatabase.connectDatabase();
    }

    public static LogInActivity getInstance() {
        return logInActivity;
    }

    public boolean checkAccount(String user, String pass) throws Exception{
        ResultSet rs = pd.query("SELECT manager_id FROM manager WHERE manager_username=\"" + user + "\" AND " + " manager_password=\"" + pass + "\";");
        return rs.next();
    }
}
