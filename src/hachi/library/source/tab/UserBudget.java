package hachi.library.source.tab;

import hachi.library.source.activity.LogInActivity;
import hachi.library.source.activity.UserActivity;
import hachi.library.source.function.FormatChecker;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by Fukie on 11/05/2016.
 */
public class UserBudget implements Initializable {
    @FXML
    private Text txtBudget;
    @FXML
    private TableView tblBudget;
    private ObservableList<UserBudget.Budget> budget;
    private int userMoney;

    @Override
    @SuppressWarnings("unchecked")
    public void initialize(URL url, ResourceBundle rb) {
        int userId = UserActivity.userId;
        TableColumn colMoney = new TableColumn("Số Tiền");
        TableColumn colDate = new TableColumn("Ngày");
        TableColumn colOrder = new TableColumn("Order");
        colMoney.setCellValueFactory(new PropertyValueFactory<UserBudget.Budget, Integer>("money"));
        colDate.setCellValueFactory(new PropertyValueFactory<UserBudget.Budget, String>("date"));
        colOrder.setCellValueFactory(new PropertyValueFactory<UserBudget.Budget, Integer>("order"));
        tblBudget.getColumns().addAll(colDate, colMoney, colOrder);
        tblBudget.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        budget = FXCollections.observableArrayList();
        try {
            ResultSet rs = LogInActivity.pd.query("SELECT history_date, history_money, order_id FROM user_history WHERE user_id='" + userId + "' ORDER BY history_date DESC");
            while (rs.next()) {
                int money = rs.getInt("history_money");
                String date = rs.getString("history_date");
                int order = rs.getInt("order_id");
                budget.add(new UserBudget.Budget(money, date, order));
            }
            tblBudget.setItems(budget);

            rs = LogInActivity.pd.query("SELECT user_money FROM user WHERE user_id='" + userId + "'");
            if (rs.next()) {
                userMoney = rs.getInt("user_money");
                txtBudget.setText(userMoney + "$");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    @SuppressWarnings("unchecked")
    protected void addBudget() {
        TextInputDialog dialog = new TextInputDialog("100");
        dialog.setTitle("Nạp Tiền");
        dialog.setHeaderText("");
        dialog.setContentText("Số tiền nạp:");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            if (FormatChecker.isNumericOnlyNumber(result.get())) {
                userMoney += Integer.parseInt(result.get());
                txtBudget.setText(userMoney + "$");
                LogInActivity.pd.queryUpdate("UPDATE user SET user_money='" + userMoney + "' WHERE user_id='" + UserActivity.userId + "'");
                String today = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
                LogInActivity.pd.queryUpdate("INSERT INTO user_history(user_id, history_date, history_money) VALUES('" + UserActivity.userId + "','"
                        + today + "','" + result.get() + "')");
                budget.add(0, new UserBudget.Budget(Integer.parseInt(result.get()), today, 0));
                tblBudget.setItems(budget);
                dialog.close();
            }
        }
    }

    public class Budget {
        private final SimpleIntegerProperty money;
        private final SimpleStringProperty date;
        private final SimpleIntegerProperty order;

        public int getMoney() {
            return money.get();
        }

        public SimpleIntegerProperty moneyProperty() {
            return money;
        }

        public void setMoney(int money) {
            this.money.set(money);
        }

        public String getDate() {
            return date.get();
        }

        public SimpleStringProperty dateProperty() {
            return date;
        }

        public void setDate(String date) {
            this.date.set(date);
        }

        public int getOrder() {
            return order.get();
        }

        public SimpleIntegerProperty orderProperty() {
            return order;
        }

        public void setOrder(int order) {
            this.order.set(order);
        }

        private Budget(int money, String date, int order) {
            this.money = new SimpleIntegerProperty(money);
            this.order = new SimpleIntegerProperty(order);
            this.date = new SimpleStringProperty(date);
        }

    }
}
