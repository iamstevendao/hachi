package hachi.library.source;

import hachi.library.source.activity.LogInActivity;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Created by Fukie on 01/05/2016.
 */
public class Main extends Application {
    public void start(Stage primaryStage) throws Exception{
       LogInActivity logInActivity = new LogInActivity();
        logInActivity.start(new Stage());
    }
    public static void main(String[] args) throws Exception{
        launch(args);
    }
}
