package hachi.library.source.tab;

import hachi.library.source.activity.BookActivity;
import hachi.library.source.activity.LogInActivity;
import hachi.library.source.activity.UserActivity;
import hachi.library.source.function.ShowBookInfomation;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;


/**
 * Created by Fukie on 09/05/2016.
 */
public class UserStatistic implements Initializable {
    @FXML
    private TextField txtSearch;
    @FXML
    private TableView tblStatistic;
    private ObservableList<UserStatistic.Statistic> statistic;
    private int userId = UserActivity.userId;

    @Override
    @SuppressWarnings("unchecked")
    public void initialize(URL url, ResourceBundle rb) {
        TableColumn colBookId = new TableColumn("Mã Sách");
        TableColumn colBookName = new TableColumn("Sách");
        TableColumn colBorrowDate = new TableColumn("Ngày mượn");
        TableColumn colReturnDate = new TableColumn("Ngày trả");
        TableColumn colOutOfDate = new TableColumn("Quá hạn");
        colBookId.setCellValueFactory(new PropertyValueFactory<UserStatistic.Statistic, Integer>("bookId"));
        colBookName.setCellValueFactory(new PropertyValueFactory<UserStatistic.Statistic, String>("bookName"));
        colBorrowDate.setCellValueFactory(new PropertyValueFactory<UserStatistic.Statistic, String>("borrowDate"));
        colReturnDate.setCellValueFactory(new PropertyValueFactory<UserStatistic.Statistic, String>("returnDate"));
        colOutOfDate.setCellValueFactory(new PropertyValueFactory<UserStatistic.Statistic, Integer>("outOfDate"));
        tblStatistic.getColumns().addAll(colBookId, colBookName, colBorrowDate, colReturnDate, colOutOfDate);
        tblStatistic.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        statistic = FXCollections.observableArrayList();
        try {
            ResultSet rs = LogInActivity.pd.query("SELECT book_id, book_name, order_register_date, line_return_date " +
                    "FROM user_order NATURAL JOIN `order` NATURAL JOIN order_line NATURAL JOIN book " +
                    "WHERE user_id='" + userId + "' ORDER BY order_register_date DESC");
            while (rs.next()) {
                int bookId = rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String borrowDate = rs.getString("order_register_date");
                String returnDate = rs.getString("line_return_date");
                int daysBetween = 0;
                if(returnDate == null) {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = df.parse(borrowDate); // conversion from String
                    java.util.Calendar cal = GregorianCalendar.getInstance();
                    cal.setTime(date);
                    cal.add(GregorianCalendar.MONTH, 1); // date manipulation
                    Date date2 = new Date();
                    if(date2.after(cal.getTime())) {
                        while (cal.getTime().before(date2)) {
                            cal.add(Calendar.DAY_OF_MONTH, 1);
                            daysBetween++;
                        }
                    }
                }
                statistic.add(new UserStatistic.Statistic(bookId, bookName, borrowDate, returnDate, daysBetween));
            }
            tblStatistic.setItems(statistic);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tblStatistic.setRowFactory( tv -> {
            TableRow<UserStatistic.Statistic> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    try {
                        Object object = tblStatistic.getSelectionModel().getSelectedItem();
                        int bookId = ((UserStatistic.Statistic)object).getBookId();
                        ShowBookInfomation.bookId=bookId;
                        ShowBookInfomation showBookInfomation=new ShowBookInfomation();
                        showBookInfomation.showBookInfo();
                    } catch (Exception e) {

                    }
                }
            });
            return row ;
        });
    }

    @FXML
    @SuppressWarnings("unchecked")
    protected void updateResultTable() throws Exception {
        statistic.clear();
        if(txtSearch.getText().length() > 0){
            ResultSet rs = LogInActivity.pd.query("SELECT book_id, book_name, order_register_date, line_return_date " +
                    "FROM user_order NATURAL JOIN `order` NATURAL JOIN order_line NATURAL JOIN book " +
                    "WHERE user_id='" + userId + "' AND book_name LIKE '%" + txtSearch.getText() + "%' ORDER BY order_register_date DESC");
            while (rs.next()) {
                int bookId = rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String borrowDate = rs.getString("order_register_date");
                String returnDate = rs.getString("line_return_date");
                int daysBetween = 0;
                if(returnDate == null) {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = df.parse(borrowDate); // conversion from String
                    java.util.Calendar cal = GregorianCalendar.getInstance();
                    cal.setTime(date);
                    cal.add(GregorianCalendar.MONTH, 1); // date manipulation
                    Date date2 = new Date();
                    if(date2.compareTo(cal.getTime()) > 0) {
                        while (cal.before(date2)) {
                            cal.add(Calendar.DAY_OF_MONTH, 1);
                            daysBetween++;
                        }
                    }
                }
                statistic.add(new UserStatistic.Statistic(bookId, bookName, borrowDate, returnDate, daysBetween));
            }
        } else {
            ResultSet rs = LogInActivity.pd.query("SELECT book_id, book_name, order_register_date, line_return_date " +
                    "FROM user_order NATURAL JOIN `order` NATURAL JOIN order_line NATURAL JOIN book " +
                    "WHERE user_id='" + userId + "' ORDER BY order_register_date DESC");
            while (rs.next()) {
                int bookId = rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String borrowDate = rs.getString("order_register_date");
                String returnDate = rs.getString("line_return_date");
                int daysBetween = 0;
                if(returnDate == null) {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = df.parse(borrowDate); // conversion from String
                    java.util.Calendar cal = GregorianCalendar.getInstance();
                    cal.setTime(date);
                    cal.add(GregorianCalendar.MONTH, 1); // date manipulation
                    Date date2 = new Date();
                    if(date2.compareTo(cal.getTime()) > 0) {
                        while (cal.before(date2)) {
                            cal.add(Calendar.DAY_OF_MONTH, 1);
                            daysBetween++;
                        }
                    }
                }
                statistic.add(new UserStatistic.Statistic(bookId, bookName, borrowDate, returnDate, daysBetween));
            }

        }
        tblStatistic.setItems(statistic);
    }

    public class Statistic {
        private final SimpleIntegerProperty bookId;

        private final SimpleStringProperty bookName;
        private final SimpleStringProperty borrowDate;
        private final SimpleStringProperty returnDate;
        private final SimpleIntegerProperty outOfDate;
        private Statistic(int bookId, String bookName, String borrowDate, String returnDate, int outOfDate) {
            this.bookId = new SimpleIntegerProperty(bookId);
            this.bookName = new SimpleStringProperty(bookName);
            this.borrowDate = new SimpleStringProperty(borrowDate);
            this.returnDate = new SimpleStringProperty(returnDate);
            this.outOfDate = new SimpleIntegerProperty(outOfDate);
        }

        public int getBookId() {
            return bookId.get();
        }

        public SimpleIntegerProperty bookIdProperty() {
            return bookId;
        }

        public void setBookId(int bookId) {
            this.bookId.set(bookId);
        }

        public String getBookName() {
            return bookName.get();
        }

        public SimpleStringProperty bookNameProperty() {
            return bookName;
        }

        public void setBookName(String bookName) {
            this.bookName.set(bookName);
        }

        public String getBorrowDate() {
            return borrowDate.get();
        }

        public SimpleStringProperty borrowDateProperty() {
            return borrowDate;
        }

        public void setBorrowDate(String borrowDate) {
            this.borrowDate.set(borrowDate);
        }

        public String getReturnDate() {
            return returnDate.get();
        }

        public SimpleStringProperty returnDateProperty() {
            return returnDate;
        }

        public void setReturnDate(String returnDate) {
            this.returnDate.set(returnDate);
        }

        public int getOutOfDate() {
            return outOfDate.get();
        }

        public SimpleIntegerProperty outOfDateProperty() {
            return outOfDate;
        }

        public void setOutOfDate(int outOfDate) {
            this.outOfDate.set(outOfDate);
        }
    }
}
