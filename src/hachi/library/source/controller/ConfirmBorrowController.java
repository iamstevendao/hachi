package hachi.library.source.controller;

import hachi.library.source.activity.ConfirmBorrowActivity;
import hachi.library.source.tab.LayoutBorrow;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Fukie on 23/04/2016.
 */
public class ConfirmBorrowController implements Initializable {
    @FXML
    protected Text txtUserName;
    @FXML
    protected Text txtBooks;
    @FXML
    protected Text txtBooksPrice;
    @FXML
    protected Text txtTotalBooksPrice;
    @FXML
    protected Text txtRemaining1;
    @FXML
    protected Text txtRemaining2;
    @FXML
    protected Text txtAdding1;
    @FXML
    protected Text txtAdding2;
    private String userName;
    private static int userMoney;
    private String[] books;
    private int[] booksPrice;
    private int totalBooksPrice;
    private int remainingMoney;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.userName = ConfirmBorrowActivity.getInstance().getUserName();
        remainingMoney = this.userMoney = ConfirmBorrowActivity.getInstance().getUserMoney();
        this.books = ConfirmBorrowActivity.getInstance().getBookName();
        this.booksPrice = ConfirmBorrowActivity.getInstance().getBookPrice();
        this.totalBooksPrice = ConfirmBorrowActivity.getInstance().getTotalBookPrice();
        txtUserName.setText(userName);
        txtBooks.setText(toTextViewFormat(books));
        txtBooksPrice.setText(toTextViewFormat(booksPrice));
        txtTotalBooksPrice.setText(Integer.toString(totalBooksPrice) + "$");
        txtRemaining2.setText(Integer.toString(userMoney) + "$");
        txtAdding2.setText(Integer.toString(totalBooksPrice) + "$");
        int diff = userMoney - totalBooksPrice;
        if (diff > 0) {
            txtRemaining1.setText(Integer.toString(diff) + "$");
            remainingMoney = diff;
            txtAdding1.setText(Integer.toString(0) + "$");
        } else {
            txtAdding1.setText(Integer.toString(-diff) + "$");
            txtRemaining1.setText(Integer.toString(0) + "$");
        }
    }

    @FXML
    protected void payAccount() throws Exception {
        ConfirmBorrowActivity.getInstance().addToOrder();
        ConfirmBorrowActivity.getInstance().payToAccount(remainingMoney, totalBooksPrice);
        LayoutBorrow.payed=true;
        destroyWindows();
    }

    @FXML
    protected void payCash() throws Exception {
        ConfirmBorrowActivity.getInstance().addToOrder();
        LayoutBorrow.payed=true;
        destroyWindows();
    }

    private String toTextViewFormat(String[] strings) {
        String rString = "";
        for (int i = 0; i < strings.length; i++) {
            if (i != 0) {
                rString += "\n";
            }
            rString += strings[i];
        }
        return rString;
    }

    private String toTextViewFormat(int[] ints) {
        String rString = "";
        for (int i = 0; i < ints.length; i++) {
            if (i != 0) {
                rString += "\n";
            }
            rString += ints[i] + "$";
        }
        return rString;
    }

    private void destroyWindows() {
        Stage stage = (Stage) txtUserName.getScene().getWindow();
        stage.close();
    }
}
