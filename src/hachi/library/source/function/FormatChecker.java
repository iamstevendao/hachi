package hachi.library.source.function;

/**
 * Created by taohansamu on 4/28/2016.
 */
public class FormatChecker {
    //  1. Chỉ có chữ số.

    public static boolean isNumericOnlyNumber(String str) {
        return str.matches("\\d+");
    }

    //2. Số nguyên dương

    public static boolean isPositiveNumber(String str) {
        return str.matches("\\+\\d+");
    }

    //  3. Số nguyên âm

    public static boolean isNegativeNumber(String str) {
        return str.matches("-\\d+");
    }

//    4. Số, số nguyên dương, số nguyên âm

    public static boolean isNumberic(String str) {
        return str.matches("[+-]?\\d+");
    }

    // contain number

    public static boolean isContainsNumber(String str) {
        return str.matches(".*\\d+.*");
    }
}
