package hachi.library.source.activity;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by taohansamu on 10/05/2016.
 */
public class BookActivity {
    private static BookActivity bookActivity;
    public static int bookId;

    public void start(Stage stage) throws Exception {
        bookActivity = this;
        Parent root = FXMLLoader.load(getClass().getResource("../../layout/book_activity.fxml"));
        Scene scene = new Scene(root, 300, 300);
        scene.getStylesheets().setAll(getClass().getResource("../../css/layout.css").toExternalForm());
        stage.setTitle("Cập Nhật Thông Tin Sách");
        stage.setScene(scene);
        stage.show();
    }

    public void updateBook(String bookName, String bookPrice, String bookAmount, String author, String bookRemaining) {
        String tmp;
        String query = "UPDATE book "
                + "SET book_name='" + bookName 
                    + "',book_price=" + bookPrice 
                    + ",book_amount=" + bookAmount 
                    + ",book_storing=" + bookRemaining
                    +",author='"+author+"'"
                + " WHERE book_id=" + bookId;
        System.out.println(query);
        LogInActivity.pd.queryUpdate(query);
        }


}
