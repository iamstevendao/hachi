package hachi.library.source.tab;

import hachi.library.source.activity.AddUserActivity;
import hachi.library.source.activity.LogInActivity;
import hachi.library.source.activity.UserActivity;
import hachi.library.source.activity.UserStatisActivity;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sun.rmi.runtime.Log;

import java.net.URL;
import java.sql.ResultSet;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by Fukie on 12/04/2016.
 */
public class LayoutUser implements Initializable {
    @FXML
    TableView tblUser;
    @FXML
    TextField txtSearchUser;
    @FXML
    Button bttnChange;
    @FXML
    Button bttnHistory;
    @FXML
    Button bttnDelete;
    ObservableList<LayoutUser.User> users;
    private static int fine_one_day;
    private static int period;
    private int userId;
    @Override
    @SuppressWarnings("unchecked")
    public void initialize(URL url, ResourceBundle rb) {
        tblUser.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tblUser.setFixedCellSize(25);
        TableColumn colUserName = new TableColumn("Bạn Đọc");
        TableColumn colUserPhone = new TableColumn("SDT");
        TableColumn colBook = new TableColumn("Số Sách");
        TableColumn colUserMoney = new TableColumn("Ngân Quỹ");
        TableColumn colBonus1 = new TableColumn("Đang mượn");
        TableColumn colBonus2 = new TableColumn("Quá hạn");
        TableColumn colMoney = new TableColumn("Tài khoản ($)");
        TableColumn colBonus3 = new TableColumn("Đang Nợ");
        TableColumn colBonus4 = new TableColumn("Thực tế");
        colBook.getColumns().addAll(colBonus1, colBonus2);
        colMoney.getColumns().addAll(colUserMoney, colBonus3, colBonus4);
        tblUser.getColumns().addAll(colUserName, colUserPhone, colBook, colMoney);
        users = FXCollections.observableArrayList();
        try {
            ResultSet rs = LogInActivity.pd.query("select * from regulations;");
            if (rs.next()) {
                fine_one_day = rs.getInt("fine_one_day");
                period = rs.getInt("period");
                fine_one_day = 1;
                period = 1;
            }
            String query = "SELECT DISTINCTROW user_id,user_name,user_phone,user_money,IFNULL(sosachdangmuon,0) as sosachdangmuon,IFNULL(sosachquahan,0) as sosachquahan,IFNULL(sotienphathientai,0) as sotienphathientai,user_money-IFNULL(sotienphathientai,0) as sotienthuc"
                    + " FROM user NATURAL LEFT JOIN ("
                    + " SELECT count(line_id) as sosachdangmuon,user_id FROM order_line natural join lib.order natural join user_order WHERE line_return_date IS NULL  GROUP BY user_id"
                    + ") as tblsosachdangmuon NATURAL LEFT JOIN ("
                    + "SELECT count(line_id) as sosachquahan,user_id  FROM order_line natural join lib.order natural join user_order WHERE line_return_date IS NULL and DATEDIFF(current_date,order_register_date)>" + period + " GROUP BY user_id"
                    + ") as tblsosachquahan NATURAL LEFT JOIN ("
                    + "SELECT sum(tienquahan1sach) as sotienphathientai,user_id FROM("
                    + "SELECT DATEDIFF(current_date,order_register_date)*" + fine_one_day + " as tienquahan1sach,user_id FROM order_line natural join lib.order natural join user_order"
                    + " WHERE line_return_date IS NULL and DATEDIFF(current_date,order_register_date)>" + period + " GROUP BY user_id"
                    + ") as tmp GROUP BY user_id"
                    + ") as tblsotienphathientai ;";

            rs = LogInActivity.pd.query(query);
            while (rs.next()) {
             //   System.out.println(query);
                userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                String userPhone = rs.getString("user_phone");
                int userMoney = rs.getInt("user_money");

                int bonus1 = rs.getInt("sosachdangmuon");
                int bonus2 = rs.getInt("sosachquahan");
                int bonus3 = rs.getInt("sotienphathientai");
                int bonus4 = rs.getInt("sotienthuc");
                users.add(new LayoutUser.User(userName, userPhone, userMoney, bonus1, bonus2, bonus3, bonus4));
            }
            colUserName.setCellValueFactory(new PropertyValueFactory<LayoutUser.User, String>("userName"));
            colUserPhone.setCellValueFactory(new PropertyValueFactory<LayoutUser.User, String>("userPhone"));
            colUserMoney.setCellValueFactory(new PropertyValueFactory<LayoutUser.User, Integer>("userMoney"));
            colBonus1.setCellValueFactory(new PropertyValueFactory<LayoutUser.User, Integer>("bonus1"));
            colBonus2.setCellValueFactory(new PropertyValueFactory<LayoutUser.User, Integer>("bonus2"));
            colBonus3.setCellValueFactory(new PropertyValueFactory<LayoutUser.User, Integer>("bonus3"));
            colBonus4.setCellValueFactory(new PropertyValueFactory<LayoutUser.User, Integer>("bonus4"));
            tblUser.setItems(users);
            tblUser.prefHeightProperty().bind(tblUser.fixedCellSizeProperty().multiply(Bindings.size(tblUser.getItems()).add(1.01)));
            tblUser.minHeightProperty().bind(tblUser.prefHeightProperty());
            tblUser.maxHeightProperty().set(25 * 7);
        } catch (Exception e) {

        }

        tblUser.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if (tblUser.getSelectionModel().getSelectedItem() != null) {
                bttnDelete.setDisable(false);
                bttnChange.setDisable(false);
                bttnHistory.setDisable(false);
            } else {
                bttnDelete.setDisable(true);
                bttnChange.setDisable(true);
                bttnHistory.setDisable(true);
            }
        });

        tblUser.setRowFactory( tv -> {
            TableRow<LayoutBorrow.Book> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    try {
                        Object object = tblUser.getSelectionModel().getSelectedItem();
                        String userName = ((User)object).getUserName();
                        ResultSet rs = LogInActivity.pd.query("SELECT user_id FROM user WHERE user_name='" + userName +"'");
                        if(rs.next()){
                            userId = rs.getInt("user_id");
                        }
                        UserActivity.userId = userId;
                        UserActivity userActivity = new UserActivity();
                        userActivity.start(new Stage());
                    } catch (Exception e) {

                    }
                }
            });
            return row ;
        });
    }

    @FXML
    protected void addUser() throws Exception {
        AddUserActivity addUserActivity = new AddUserActivity();
        addUserActivity.start(new Stage());
    }

    @FXML
    protected void changeUser() {
        try {
            Object object = tblUser.getSelectionModel().getSelectedItem();
            String userName = ((User)object).getUserName();
            ResultSet rs = LogInActivity.pd.query("SELECT user_id FROM user WHERE user_name='" + userName +"'");
            if(rs.next()){
                userId = rs.getInt("user_id");
            }
            UserActivity.userId = userId;
            UserActivity userActivity = new UserActivity();
            userActivity.start(new Stage());
        } catch (Exception e) {

        }
    }

    @FXML
    protected void deleteUser() {
        Object object = tblUser.getSelectionModel().getSelectedItem();
        String userName = ((User) object).getUserName();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Xóa người dùng");
        alert.setHeaderText(null);
        alert.setContentText("Xóa bạn đọc " + userName + "?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            String userPhone = ((User) object).getUserPhone();
            LogInActivity.pd.queryUpdate("DELETE FROM user WHERE user_name ='" + userName + "' AND user_phone ='" + userPhone + "';");
            alert.close();
        } else {
            alert.close();
        }
    }

    @FXML
    protected void analyzeUser() throws Exception {
        try {
            Object object = tblUser.getSelectionModel().getSelectedItem();
            String userName = ((User)object).getUserName();
            ResultSet rs = LogInActivity.pd.query("SELECT user_id FROM user WHERE user_name='" + userName +"'");
            if(rs.next()){
                userId = rs.getInt("user_id");
            }
            UserActivity.userId = userId;
        } catch (Exception e) {

        }
        UserStatisActivity userStatisActivity=new UserStatisActivity();
        userStatisActivity.start(new Stage());
    }

    @FXML
    @SuppressWarnings("unchecked")
    protected void updateResultTable() throws Exception {
        users.clear();
        if (txtSearchUser.getText().length() > 0) {
            String query = "SELECT DISTINCTROW user_id,user_name,user_phone,user_money,IFNULL(sosachdangmuon,0) as sosachdangmuon,IFNULL(sosachquahan,0) as sosachquahan,IFNULL(sotienphathientai,0) as sotienphathientai,user_money-IFNULL(sotienphathientai,0) as sotienthuc"
                    + " FROM user NATURAL LEFT JOIN ("
                    + " SELECT count(line_id) as sosachdangmuon,user_id FROM order_line natural join lib.order natural join user_order WHERE line_return_date IS NULL  GROUP BY user_id"
                    + ") as tblsosachdangmuon NATURAL LEFT JOIN ("
                    + "SELECT count(line_id) as sosachquahan, user_id  FROM order_line natural join lib.order natural join user_order WHERE line_return_date IS NULL and DATEDIFF(current_date,order_register_date)>" + period + " GROUP BY user_id"
                    + ") as tblsosachquahan NATURAL LEFT JOIN ("
                    + "SELECT sum(tienquahan1sach) as sotienphathientai,user_id FROM("
                    + "SELECT DATEDIFF(current_date,order_register_date)*" + fine_one_day + " as tienquahan1sach,user_id FROM order_line natural join lib.order natural join user_order"
                    + " WHERE line_return_date IS NULL and DATEDIFF(current_date,order_register_date)>" + period + " GROUP BY user_id"
                    + ") as tmp GROUP BY user_id"
                    + ") as tblsotienphathientai "
                    + " WHERE user_name LIKE '%"
                    + txtSearchUser.getText()
                    + "%';";
//            System.out.println(query);
            ResultSet rs = LogInActivity.pd.query(query);
            while (rs.next()) {
                String userName = rs.getString("user_name");
                String userPhone = rs.getString("user_phone");
                int userMoney = rs.getInt("user_money");
                int bonus1 = rs.getInt("sosachdangmuon");
                int bonus2 = rs.getInt("sosachquahan");
                int bonus3 = rs.getInt("sotienphathientai");
                int bonus4 = rs.getInt("sotienthuc");
                users.add(new LayoutUser.User(userName, userPhone, userMoney, bonus1, bonus2, bonus3, bonus4));
            }
        } else {
            String query = "SELECT DISTINCTROW user_id,user_name,user_phone,user_money,IFNULL(sosachdangmuon,0) as sosachdangmuon,IFNULL(sosachquahan,0) as sosachquahan,IFNULL(sotienphathientai,0) as sotienphathientai,user_money-IFNULL(sotienphathientai,0) as sotienthuc"
                    + " FROM user NATURAL LEFT JOIN ("
                    + " SELECT count(line_id) as sosachdangmuon,user_id FROM order_line natural join lib.order natural join user_order WHERE line_return_date IS NULL  GROUP BY user_id"
                    + ") as tblsosachdangmuon NATURAL LEFT JOIN ("
                    + "SELECT count(line_id) as sosachquahan,user_id  FROM order_line natural join lib.order natural join user_order WHERE line_return_date IS NULL and DATEDIFF(current_date,order_register_date)>" + period + " GROUP BY user_id"
                    + ") as tblsosachquahan NATURAL LEFT JOIN ("
                    + "SELECT sum(tienquahan1sach) as sotienphathientai,user_id FROM("
                    + "SELECT DATEDIFF(current_date,order_register_date)*" + fine_one_day + " as tienquahan1sach,user_id FROM order_line natural join lib.order natural join user_order"
                    + " WHERE line_return_date IS NULL and DATEDIFF(current_date,order_register_date)>" + period + " GROUP BY user_id"
                    + ") as tmp GROUP BY user_id"
                    + ") as tblsotienphathientai ;";
            ResultSet rs = LogInActivity.pd.query(query);
            while (rs.next()) {
                String userName = rs.getString("user_name");
                String userPhone = rs.getString("user_phone");
                int userMoney = rs.getInt("user_money");
                int bonus1 = rs.getInt("sosachdangmuon");
                int bonus2 = rs.getInt("sosachquahan");
                int bonus3 = rs.getInt("sotienphathientai");
                int bonus4 = rs.getInt("sotienthuc");
                users.add(new LayoutUser.User(userName, userPhone, userMoney, bonus1, bonus2, bonus3, bonus4));
            }
        }
        tblUser.setItems(users);
    }

    public static class User {
        private final SimpleStringProperty userName;
        private final SimpleStringProperty userPhone;
        private final SimpleIntegerProperty userMoney;
        private final SimpleIntegerProperty bonus1;
        private final SimpleIntegerProperty bonus2;
        private final SimpleIntegerProperty bonus3;
        private final SimpleIntegerProperty bonus4;

        private User(String userName, String userPhone, int userMoney, int bonus1, int bonus2, int bonus3, int bonus4) {
            this.userName = new SimpleStringProperty(userName);
            this.userPhone = new SimpleStringProperty(userPhone);
            this.userMoney = new SimpleIntegerProperty(userMoney);
            this.bonus1 = new SimpleIntegerProperty(bonus1);
            this.bonus2 = new SimpleIntegerProperty(bonus2);
            this.bonus3 = new SimpleIntegerProperty(bonus3);
            this.bonus4 = new SimpleIntegerProperty(bonus4);
        }

        public String getUserName() {
            return userName.get();
        }

        public SimpleStringProperty userNameProperty() {
            return userName;
        }

        public int getUserMoney() {
            return userMoney.get();
        }

        public SimpleIntegerProperty userMoneyProperty() {
            return userMoney;
        }

        public String getUserPhone() {
            return userPhone.get();
        }

        public SimpleStringProperty userPhoneProperty() {
            return userPhone;
        }

        public int getBonus1() {
            return bonus1.get();
        }

        public SimpleIntegerProperty bonus1Property() {
            return bonus1;
        }

        public int getBonus2() {
            return bonus2.get();
        }

        public SimpleIntegerProperty bonus2Property() {
            return bonus2;
        }

        public int getBonus3() {
            return bonus3.get();
        }

        public SimpleIntegerProperty bonus3Property() {
            return bonus3;
        }

        public int getBonus4() {
            return bonus4.get();
        }

        public SimpleIntegerProperty bonus4Property() {
            return bonus4;
        }
    }
}
