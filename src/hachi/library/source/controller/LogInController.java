package hachi.library.source.controller;

import hachi.library.source.activity.LogInActivity;
import hachi.library.source.activity.MainActivity;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LogInController {
    @FXML private Text actiontarget;
    @FXML private TextField txtUsername;
    @FXML private PasswordField txtPassword;

    @FXML
    protected void handleSubmitButtonAction() throws Exception {
        if(txtUsername.getText().equals("")){
            actiontarget.setText("Nhap Username");
        } else if(txtPassword.getText().equals("")){
            actiontarget.setText("Nhap Password");
        } else {
            if(LogInActivity.getInstance().checkAccount(txtUsername.getText(), txtPassword.getText())) {
                MainActivity mainActivity = new MainActivity();
                mainActivity.start(new Stage());
                Stage stage = (Stage) actiontarget.getScene().getWindow();
                stage.close();
            } else {
                actiontarget.setText("Sai tai khoan hoac mat khau");
            }
        }
    }
    @FXML
    protected void enterPressed(KeyEvent event) throws Exception {
        if(event.getCode()== KeyCode.ENTER)
            handleSubmitButtonAction();
    }
}
