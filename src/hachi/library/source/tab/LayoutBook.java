package hachi.library.source.tab;

import hachi.library.source.activity.AddBookActivity;
import hachi.library.source.activity.BookActivity;
import hachi.library.source.activity.LogInActivity;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by Fukie on 11/04/2016.
 */
public class LayoutBook implements Initializable {
    @FXML
    private TableView tblBook;
    @FXML
    private TextField txtSearchBook;
    ObservableList<LayoutBook.Book> books;
    @FXML
    private Button btnDelete;
    private static boolean initialized = false;
    @Override
    @SuppressWarnings("unchecked")
    public void initialize(URL url, ResourceBundle rb) {
        initTblBook();
    }
    @SuppressWarnings("unchecked")
    private  void initTblBook(){
        if(!initialized){
            TableColumn colBookId=new TableColumn("Mã Sách");
            TableColumn colBookName = new TableColumn("Sách");
            TableColumn colAuthorName = new TableColumn("Tác Giả");
            TableColumn colBookStoring = new TableColumn("Số Lượng Trong Kho");
            colBookId.setCellValueFactory(new PropertyValueFactory<LayoutBook.Book, Integer>("bookId"));
            colBookName.setCellValueFactory(new PropertyValueFactory<LayoutBook.Book, String>("bookName"));
            colAuthorName.setCellValueFactory(new PropertyValueFactory<LayoutBook.Book, String>("authorName"));
            colBookStoring.setCellValueFactory(new PropertyValueFactory<LayoutBook.Book, Integer>("bookStoring"));
            tblBook.getColumns().addAll(colBookId,colBookName, colAuthorName, colBookStoring);
            tblBook.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            initialized=true;
        }
        books = FXCollections.observableArrayList();
        books.clear();
        try {
            ResultSet rs = LogInActivity.pd.query("SELECT book_id ,book_name, author, book_storing FROM book  ORDER BY book_id");
            while (rs.next()) {
                int bookId=rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String authorName = rs.getString("author");
                int bookStoring = rs.getInt("book_storing");
                books.add(new Book(bookId,bookName, authorName, bookStoring));
            }
            tblBook.setItems(books);
        } catch (Exception e) {

        }
        tblBook.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if (tblBook.getSelectionModel().getSelectedItem() != null) {
                btnDelete.setDisable(false);
            } else {
                btnDelete.setDisable(true);
            }
        });

        tblBook.setRowFactory( tv -> {
            TableRow<Book> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    try {
                        Object object = tblBook.getSelectionModel().getSelectedItem();
                        int bookId = ((Book)object).getBookId();
                        BookActivity.bookId = bookId;
                        BookActivity bookActivity = new BookActivity();
                        bookActivity.start(new Stage());
                    } catch (Exception e) {

                    }
                }
            });
            return row ;
        });
        tblBook.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }
    @FXML
    protected void addBook() throws Exception {
        AddBookActivity addBookActivity = new AddBookActivity();
        addBookActivity.start(new Stage());
    }

    @FXML
    protected void changeBook() {

    }


    @FXML
    protected void analyzeBook() {

    }

    @FXML
    @SuppressWarnings("unchecked")
    protected void updateResultTable() throws Exception {
        books.clear();
        if(txtSearchBook.getText().length() > 0) {
            String query="SELECT book_id, book_name, author, book_storing " +
                    "FROM book WHERE book_id LIKE '%"+txtSearchBook.getText()
                    + "%' OR book_name LIKE '%"
                    + txtSearchBook.getText()
                    + "%' OR author LIKE '%"
                    + txtSearchBook.getText()
                    + "%' ORDER BY book_id;";
            System.out.println(query);
            ResultSet rs = LogInActivity.pd.query(query);
            while (rs.next()) {
                int bookId=rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String authorName = rs.getString("author");
                int bookAmount = rs.getInt("book_storing");
                books.add(new Book(bookId,bookName, authorName, bookAmount));
                tblBook.prefHeightProperty().bind(tblBook.fixedCellSizeProperty().multiply(Bindings.size(tblBook.getItems()).add(1.01)));
            }
        } else {
            ResultSet rs = LogInActivity.pd.query("SELECT book_id, book_name, author, book_storing FROM book ORDER BY book_id");
            while (rs.next()) {
                int bookId=rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String authorName = rs.getString("author");
                int bookStoring = rs.getInt("book_storing");
                books.add(new Book(bookId,bookName, authorName, bookStoring));
            }
        }
        tblBook.setItems(books);
    }

    @FXML
    protected void deleteBook() {
        Object object = tblBook.getSelectionModel().getSelectedItem();
        String bookName=((LayoutBook.Book) object).getBookName();
        int bookId = ((LayoutBook.Book) object).getBookId();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Xóa sách");
        alert.setHeaderText(null);
        alert.setContentText("Xóa sách " + bookName + "?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            LogInActivity.pd.queryUpdate("DELETE FROM book WHERE book_id =" + bookId + ";");
            initTblBook();
            alert.close();
        } else {
            alert.close();
        }
    }

    public void refresh(ActionEvent actionEvent) throws SQLException {
        initTblBook();
    }

    public class Book {
        private final SimpleIntegerProperty bookId ;
        private final SimpleStringProperty bookName;
        private final SimpleStringProperty authorName;
        private final SimpleIntegerProperty bookStoring;

        private Book(int bookId,String bookName, String authorName, int bookStoring) {
            this.bookName = new SimpleStringProperty(bookName);
            this.authorName = new SimpleStringProperty(authorName);
            this.bookStoring = new SimpleIntegerProperty(bookStoring);
            this.bookId = new SimpleIntegerProperty(bookId);
        }
        public int getBookId()
        {
            return bookId.get();
        }
        public SimpleIntegerProperty bookIdProperty() {
            return bookId;
        }

        public String getBookName() {
            return bookName.get();
        }

        public SimpleStringProperty bookNameProperty() {
            return bookName;
        }

        public int getBookStoring() {
            return bookStoring.get();
        }

        public SimpleIntegerProperty bookStoringProperty() {
            return bookStoring;
        }

        public String getAuthorName() {
            return authorName.get();
        }

        public SimpleStringProperty authorNameProperty() {
            return authorName;
        }
    }
}
