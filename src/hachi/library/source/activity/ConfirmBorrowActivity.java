package hachi.library.source.activity;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Fukie on 23/04/2016.
 */
public class ConfirmBorrowActivity extends Application {
    private static ConfirmBorrowActivity confirmBorrowActivity;
    private int userId;
    private String userName;
    private int userMoney;
    private String[] bookName;
    private int[] bookPrice;
    private int[] bookId;
    private int countOrder;

    @Override
    public void start(Stage stage) throws Exception {
        confirmBorrowActivity = this;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../../layout/confirm_borrow.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        scene.getStylesheets().setAll(getClass().getResource("../../css/layout.css").toExternalForm());
        stage.setTitle("Hoa don");
        stage.setScene(scene);
        stage.show();
    }

    public ConfirmBorrowActivity(int userId, String userName, int userMoney, int[] bookId, String[] bookName, int[] bookPrice) {
        this.userId = userId;
        this.userName = userName;
        this.userMoney = userMoney;
        this.bookName = bookName;
        this.bookPrice = bookPrice;
        this.bookId = bookId;
    }

    public static ConfirmBorrowActivity getInstance() {
        return confirmBorrowActivity;
    }

    public int getUserMoney() {
        return userMoney;
    }

    public String getUserName() {
        return userName;
    }

    public String[] getBookName() {
        return bookName;
    }

    public int[] getBookPrice() {
        return bookPrice;
    }

    public void addToOrder() throws Exception {
        ResultSet rs = LogInActivity.pd.query("SELECT MAX(order_id) FROM `order`;");
        if (rs.next()) {
            countOrder = rs.getInt("MAX(order_id)") + 1;
        } else countOrder = 1;
        String today = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        String addOrder = "INSERT INTO `order`(`order_id`, `order_register_date`) VALUES('" + countOrder + "','" + today + "');";
        String addUserOrder = "INSERT INTO `user_order`(user_id, order_id) VALUES('" + userId + "','" + countOrder + "');";
        String addOrderLine = "";
        //rs = LogInActivity.pd.query("SELECT MAX(line_id) FROM line;");
        //rs.next();
        int countLine = 1;
        for (int bookIdTmp : bookId) {
            addOrderLine += "INSERT INTO `order_line`(order_id, line_id, book_id) VALUES('" + countOrder + "','" + countLine + "','" + bookIdTmp + "');";
            countLine++;
        }
        LogInActivity.pd.queryUpdate(addOrder + addUserOrder + addOrderLine);
    }

    public void payToAccount(int remaining, int totalPrice) throws Exception {
        String today = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        LogInActivity.pd.queryUpdate("UPDATE user SET user_money = " + remaining + " WHERE user_id = " + userId);
        LogInActivity.pd.queryUpdate("INSERT INTO user_history VALUES('" + userId + "','"
                + today + "','-" + totalPrice + "','" + countOrder + "')");
    }

    public int getTotalBookPrice() {
        int rInt = 0;
        for (int money : bookPrice) {
            rInt += money;
        }
        return rInt;
    }
}
