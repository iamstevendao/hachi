﻿-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: lib
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `book_id` int(11) NOT NULL,
  `book_name` varchar(100) NOT NULL,
  `book_price` int(11) DEFAULT '0',
  `book_amount` int(11) DEFAULT '0',
  `book_storing` int(11) DEFAULT '0',
  `book_borrow_times` int(11) DEFAULT '0',
  `author` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'Never Let Me Go',76,40,22,4,'Kazuo Ishigur'),(2,'Saturday',80,100,100,0,'Ian McEwa'),(3,'On Beauty',62,30,30,0,'Zadie Smit'),(4,'Slow Man',61,46,46,0,'J.M. Coetze'),(5,'Adjunct: An Undigest',58,56,56,0,'Peter Manso'),(6,'The Sea',85,4,4,0,'John Banvill'),(7,'The Red Queen',51,56,56,0,'Margaret Drabbl'),(8,'The Plot Against America',68,7,7,0,'Philip Rot'),(9,'The Master',40,6,6,0,'Colm Tóibí'),(10,'Vanishing Point',96,100,100,0,'David Markso'),(11,'The Lambs of London',2,100,100,0,'Peter Ackroy'),(12,'Dining on Stones',79,100,100,0,'Iain Sinclai'),(13,'Cloud Atlas',28,100,100,0,'David Mitchel'),(14,'Drop City',18,100,100,0,'T. Coraghessan Boyl'),(15,'The Colour',23,100,100,0,'Rose Tremai'),(16,'Thursbitch',78,100,100,0,'Alan Garne'),(17,'The Light of Day',77,100,100,0,'Graham Swif'),(18,'What I Loved',94,100,100,0,'Siri Hustved'),(19,'The Curious Incident of the Dog in the Night-Time',62,100,100,0,'Mark Haddo'),(20,'Islands',70,100,100,0,'Dan Sleig'),(21,'Elizabeth Costello',23,100,100,0,'J.M. Coetze'),(22,'London Orbital',63,100,100,0,'Iain Sinclai'),(23,'Family Matters',18,100,100,0,'Rohinton Mistr'),(24,'Fingersmith',7,100,100,0,'Sarah Water'),(25,'The Double',91,100,100,0,'José Saramag'),(26,'Everything is Illuminated',90,100,100,0,'Jonathan Safran Foe'),(27,'Unless',29,100,100,0,'Carol Shield'),(28,'Kafka on the Shore',8,100,100,0,'Haruki Murakam'),(29,'The Story of Lucy Gault',14,100,100,0,'William Trevo'),(30,'That They May Face the Rising Sun',90,100,100,0,'John McGaher'),(31,'In the Forest',20,100,100,0,'Edna O’Brie'),(32,'Shroud',55,100,100,0,'John Banvill'),(33,'Middlesex',40,100,100,0,'Jeffrey Eugenide'),(34,'Youth',81,100,100,0,'J.M. Coetze'),(35,'Dead Air',56,100,100,0,'Iain Bank'),(36,'Nowhere Man',59,100,100,0,'Aleksandar Hemo'),(37,'The Book of Illusions',46,100,100,0,'Paul Auste'),(38,'Gabriel’s Gift',62,100,100,0,'Hanif Kureish'),(39,'Austerlitz',18,100,100,0,'W.G. Sebal'),(40,'Platform',19,100,100,0,'Michael Houellebec'),(41,'Schooling',43,100,100,0,'Heather McGowa'),(42,'Atonement',67,100,100,0,'Ian McEwa'),(43,'The Corrections',53,100,100,0,'Jonathan Franze'),(44,'Don’t Move',26,100,100,0,'Margaret Mazzantin'),(45,'The Body Artist',14,100,100,0,'Don DeLill'),(46,'Fury',72,100,100,0,'Salman Rushdi'),(47,'At Swim, Two Boys',47,100,100,0,'Jamie O’Neil'),(48,'Choke',62,100,100,0,'Chuck Palahniu'),(49,'Life of Pi',38,100,100,0,'Yann Marte'),(50,'The Feast of the Goat',72,100,100,0,'Mario Vargos Llos'),(51,'An Obedient Father',61,100,100,0,'Akhil Sharm'),(52,'The Devil and Miss Prym',9,100,100,0,'Paulo Coelh'),(53,'Spring Flowers, Spring Frost',1,100,100,0,'Ismail Kadar'),(54,'White Teeth',69,100,100,0,'Zadie Smit'),(55,'The Heart of Redness',27,100,100,0,'Zakes Md'),(56,'Under the Skin',28,100,100,0,'Michel Fabe'),(57,'Ignorance',37,100,100,0,'Milan Kunder'),(58,'Nineteen Seventy Seven',45,100,100,0,'David Peac'),(59,'Celestial Harmonies',73,100,100,0,'Péter Esterház'),(60,'City of God',76,100,100,0,'E.L. Doctoro'),(61,'How the Dead Live',92,100,100,0,'Will Sel'),(62,'The Human Stain',93,100,100,0,'Philip Rot'),(63,'The Blind Assassin',28,100,100,0,'Margaret Atwoo'),(64,'After the Quake',5,100,100,0,'Haruki Murakam'),(65,'Small Remedies',93,100,100,0,'Shashi Deshpand'),(66,'Super-Cannes',99,100,100,0,'J.G. Ballar'),(67,'House of Leaves',66,100,100,0,'Mark Z. Danielewsk'),(68,'Blonde',63,100,100,0,'Joyce Carol Oate'),(69,'Pastoralia',9,100,100,0,'George Saunder'),(70,'Timbuktu',54,100,100,0,'Paul Auste'),(71,'The Romantics',95,100,100,0,'Pankaj Mishr'),(72,'Cryptonomicon',43,100,100,0,'Neal Stephenso'),(73,'As If I Am Not There',58,100,100,0,'Slavenka Drakuli'),(74,'Everything You Need',25,100,100,0,'A.L. Kenned'),(75,'Fear and Trembling',5,100,100,0,'Amélie Nothom'),(76,'The Ground Beneath Her Feet',95,100,100,0,'Salman Rushdi'),(77,'Disgrace',31,100,100,0,'J.M. Coetze'),(78,'Sputnik Sweetheart',58,100,100,0,'Haruki Murakam'),(79,'Elementary Particles',31,100,100,0,'Michel Houellebec'),(80,'Intimacy',59,100,100,0,'Hanif Kureish'),(81,'Amsterdam',14,100,100,0,'Ian McEwa'),(82,'Cloudsplitter',96,100,100,0,'Russell Bank'),(83,'All Souls Day',33,100,100,0,'Cees Nooteboo'),(84,'The Talk of the Town',9,100,100,0,'Ardal O’Hanlo'),(85,'Tipping the Velvet',68,100,100,0,'Sarah Water'),(86,'The Poisonwood Bible',91,100,100,0,'Barbara Kingsolve'),(87,'Glamorama',66,100,100,0,'Bret Easton Elli'),(88,'Another World',20,100,100,0,'Pat Barke'),(89,'The Hours',42,100,100,0,'Michael Cunningha'),(90,'Veronika Decides to Die',51,100,100,0,'Paulo Coelh'),(91,'Mason & Dixon',32,100,100,0,'Thomas Pyncho'),(92,'The God of Small Things',94,100,100,0,'Arundhati Ro'),(93,'Memoirs of a Geisha',59,100,100,0,'Arthur Golde'),(94,'Great Apes',57,100,100,0,'Will Sel'),(95,'Enduring Love',53,100,100,0,'Ian McEwa'),(96,'Underworld',59,100,100,0,'Don DeLill'),(97,'Jack Maggs',34,100,100,0,'Peter Care'),(98,'The Life of Insects',59,100,100,0,'Victor Pelevi'),(99,'American Pastoral',68,100,100,0,'Philip Rot'),(100,'The Untouchable',4,100,100,0,'John Banvill'),(101,'Hello',58,70,70,0,'Adele');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dayout_fine`
--

DROP TABLE IF EXISTS `dayout_fine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dayout_fine` (
  `fine_1day` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dayout_fine`
--

LOCK TABLES `dayout_fine` WRITE;
/*!40000 ALTER TABLE `dayout_fine` DISABLE KEYS */;
INSERT INTO `dayout_fine` VALUES (3);
/*!40000 ALTER TABLE `dayout_fine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `manager_id` int(11) NOT NULL,
  `manager_username` varchar(45) NOT NULL,
  `manager_password` varchar(45) NOT NULL,
  PRIMARY KEY (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager`
--

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
INSERT INTO `manager` VALUES (1,'f','f');
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_register_date` date DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,'2016-05-04'),(2,'2016-04-03'),(3,'2016-05-11'),(4,'2016-05-11'),(5,'2016-05-11'),(6,'2016-05-12'),(7,'2016-05-12'),(8,'2016-05-12');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_line`
--

DROP TABLE IF EXISTS `order_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_line` (
  `order_id` int(11) NOT NULL,
  `line_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `line_return_date` date DEFAULT NULL,
  `line_out_dates` int(11) DEFAULT '0',
  `line_profit` int(11) DEFAULT '0',
  PRIMARY KEY (`order_id`,`line_id`),
  KEY `fk_line_id_idx` (`line_id`),
  KEY `fk_book_id2` (`book_id`),
  CONSTRAINT `fk_book_id2` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_id` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_line`
--

LOCK TABLES `order_line` WRITE;
/*!40000 ALTER TABLE `order_line` DISABLE KEYS */;
INSERT INTO `order_line` VALUES (1,1,1,NULL,0,0),(1,2,2,NULL,0,0),(1,3,3,NULL,0,0),(1,4,4,NULL,2,16),(1,5,5,NULL,0,0),(2,1,1,NULL,0,0),(2,2,2,NULL,0,0),(2,3,3,NULL,0,0),(2,4,4,NULL,0,0),(5,1,1,NULL,0,0),(5,2,13,NULL,0,0),(6,1,1,'2016-05-12',0,0),(7,1,1,'2016-05-12',0,0),(8,1,1,'2016-05-12',0,0);
/*!40000 ALTER TABLE `order_line` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `lib`.`order_line_AFTER_INSERT` AFTER INSERT ON `order_line` FOR EACH ROW
BEGIN
	UPDATE book SET book_storing = book_storing - 1, book_borrow_times = book_borrow_times + 1
    WHERE book_id = NEW.book_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `lib`.`order_line_AFTER_UPDATE` AFTER UPDATE ON `order_line` FOR EACH ROW
BEGIN
	UPDATE book SET book_storing = book_storing + 1
    WHERE book_id = NEW.book_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `regulations`
--

DROP TABLE IF EXISTS `regulations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regulations` (
  `fine_one_day` int(11) NOT NULL DEFAULT '3',
  `period` int(11) NOT NULL DEFAULT '30'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regulations`
--

LOCK TABLES `regulations` WRITE;
/*!40000 ALTER TABLE `regulations` DISABLE KEYS */;
INSERT INTO `regulations` VALUES (3,30);
/*!40000 ALTER TABLE `regulations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) NOT NULL,
  `user_password` varchar(45) NOT NULL,
  `user_address` varchar(45) DEFAULT NULL,
  `user_phone` varchar(11) DEFAULT NULL,
  `user_money` int(11) DEFAULT '0',
  `user_email` varchar(45) DEFAULT NULL,
  `user_register_date` date DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Đào Phú Kiên','1','123VN','01283461324',10871,'kien@gmail','2016-04-13'),(2,'Nguyễn Đình Tạo','1',NULL,'0123456789',10279,NULL,'2016-04-18'),(3,'Nguyễn Hữu Toàn','1',NULL,'0987654321',1000,NULL,'2016-04-14'),(4,'Nguyễn Văn Hưng','1',NULL,'0123521233',2924,NULL,'2016-04-16'),(5,'Đới Khắc Thành','1',NULL,'0564564566',200,NULL,'2016-04-30');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_history`
--

DROP TABLE IF EXISTS `user_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_history` (
  `user_id` int(11) NOT NULL,
  `history_date` date NOT NULL,
  `history_money` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`history_date`,`history_money`),
  KEY `user_order_history_fk_idx` (`order_id`),
  CONSTRAINT `user_history_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_order_history_fk` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_history`
--

LOCK TABLES `user_history` WRITE;
/*!40000 ALTER TABLE `user_history` DISABLE KEYS */;
INSERT INTO `user_history` VALUES (1,'2016-05-11',100,NULL),(1,'2016-05-12',100,NULL),(1,'2016-05-12',200,NULL),(1,'2016-05-11',-104,5),(3,'2016-05-12',-76,6),(4,'2016-05-12',-76,7),(5,'2016-05-12',-76,8);
/*!40000 ALTER TABLE `user_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_order`
--

DROP TABLE IF EXISTS `user_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_order` (
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`order_id`),
  KEY `fk_order_id2_idx` (`order_id`),
  CONSTRAINT `fk_order_id2` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_order`
--

LOCK TABLES `user_order` WRITE;
/*!40000 ALTER TABLE `user_order` DISABLE KEYS */;
INSERT INTO `user_order` VALUES (1,1),(2,2),(1,5),(3,6),(4,7),(5,8);
/*!40000 ALTER TABLE `user_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'lib'
--

--
-- Dumping routines for database 'lib'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

<<<<<<< HEAD
-- Dump completed on 2016-05-18  0:30:23
=======
-- Dump completed on 2016-05-16 17:19:27
>>>>>>> origin/test
