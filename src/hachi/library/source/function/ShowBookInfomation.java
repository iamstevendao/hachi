package hachi.library.source.function;

import hachi.library.source.activity.LogInActivity;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

/**
 * Created by taohansamu on 5/11/2016.
 */
public class ShowBookInfomation implements Initializable {

    public static int bookId;
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    public void showBookInfo() throws Exception{
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        ResultSet rs= LogInActivity.pd.query("SELECT * FROM book  WHERE book_id="+bookId);
        if(rs.next()){
            alert.setTitle(rs.getString("book_name").toUpperCase());
            alert.setHeaderText(rs.getString("book_name").toUpperCase()+" (Mã: "+rs.getInt("book_id")+")");
            alert.setContentText("Tác Giả           : "+rs.getString("author")+"\n"
                                +"Giá Sách          : "+rs.getInt("book_price")+"\n"
                                +"Còn               : "+rs.getInt("book_storing")+"/"+rs.getInt("book_amount")+"\n"
                                +"Số lượt mượn      : "+rs.getInt("book_borrow_times")+"\n");
            alert.showAndWait();
        }


    }

}
