package hachi.library.source.activity;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.ResultSet;

/**
 * Created by taohansamu on 4/23/2016.
 */

public class AddBookActivity extends Application{
    public void start(Stage stage) throws Exception
    {
        Parent root= FXMLLoader.load(getClass().getResource("../../layout/add_book.fxml"));
        Scene scene= new Scene(root,430,480);
        stage.setTitle("Them Sach");
        stage.setScene(scene);
        stage.show();
    }

    public static boolean bookIsExist(String bookId) throws Exception {
        String query = "SELECT * FROM book WHERE book_id=" + bookId + ";";
        ResultSet rs = LogInActivity.pd.query(query);
        return rs.next();
    }
    public void addBook(String bookId,String bookName,String bookPrice,String amount,String author) throws Exception
    {
        String query="INSERT INTO book VALUES("+bookId+",\""+bookName+"\","+bookPrice+","+amount+","+amount+",0,'"+author+"');";
        System.out.print(query);
        LogInActivity.pd.queryUpdate(query);
    }
}
