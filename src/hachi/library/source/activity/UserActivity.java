package hachi.library.source.activity;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Fukie on 09/05/2016.
 */
public class UserActivity {
    private static UserActivity userActivity;
    public static int userId;
    public void start(Stage stage) throws Exception {
        userActivity = this;
        Parent root = FXMLLoader.load(getClass().getResource("../../layout/user_activity.fxml"));
        Scene scene = new Scene(root, 500, 400);
        scene.getStylesheets().setAll(getClass().getResource("../../css/layout.css").toExternalForm());
        stage.setTitle("Thog tin ng dung");
        stage.setScene(scene);
        stage.show();
    }
}
