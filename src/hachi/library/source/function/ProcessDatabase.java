package hachi.library.source.function;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Random;

/**
 * Created by Fukie on 30/03/2016.
 */
public class ProcessDatabase {
    static private Connection connection;
    private Statement statement;

    private ProcessDatabase(String url, String username, String password) {
        try {
            connection = DriverManager.getConnection(url, username, password);
            System.out.println("Database connected!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static ProcessDatabase connectDatabase() {
        return new ProcessDatabase("jdbc:mysql://localhost:3306/lib?useSSL=false", "root", "hachi");
    }

    public ResultSet query(String string) {
        try {
            statement = connection.createStatement();
            return statement.executeQuery(string);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void queryUpdate(String string) {
        try {
            statement = connection.createStatement();
            String[] strings = string.split(";");
            for(String tmp : strings){
                if(tmp.length() > 1) {
                    statement.executeUpdate(tmp);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void initDatabase() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("F:/hachi/src/hachi/database/bookdata.txt"));
            String line = br.readLine();
            int kt = 0;
            while (line != null) {
                //String[] result = line.split("\\s+-\\s+");
                int i = 0;
                int state = 0;
                int pos = 0;
                String[] result = new String[2];
                char x;
                while ((x = line.charAt(i)) != '\n') {
                    if (state == 1) {
                        if (x == 32) {
                            pos = i;
                            break;
                        }
                    }
                    if (x == 8211) {
                        state = 1;
                    } else state = 0;
                    i++;
                    if (i == line.length()) {
                        kt = 1;
                        break;
                    }
                }
                if (kt == 0) {
                    if (pos != 0) {
                        result[0] = line.substring(0, pos - 2);
                        result[1] = line.substring(pos + 1, line.length() - 1);
                    }
                    Random rd = new Random();
                    int price = rd.nextInt(100) + 1;
                    if (result.length == 2) {
                        addBook(result[0], price, result[1]);
                    }
                }
                line = br.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addBook(String bookName, int bookPrice, String authorName) {
        try {
            ResultSet rs = query("SELECT COUNT(book_id) FROM book");
            rs.next();
            int bookId = rs.getInt("COUNT(book_id)") + 1;
            rs = query("SELECT author_id FROM author WHERE author_name=\"" + authorName + "\"");
            int authorId;
            if (rs.next()) {
                authorId = rs.getInt("author_id");
            } else {
                rs = query("SELECT COUNT(author_id) FROM author");
                rs.next();
                authorId = rs.getInt("COUNT(author_id)") + 1;
                queryUpdate("INSERT INTO author(author_id, author_name) VALUES(" + authorId + ",\"" + authorName + "\");");
            }
            queryUpdate("INSERT INTO book(book_id, book_name, book_price) VALUES(" + bookId + ",\"" + bookName + "\"," + bookPrice + ");");
            queryUpdate("INSERT INTO author_book(book_id, author_id) VALUES(" + bookId + "," + authorId + ");");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
