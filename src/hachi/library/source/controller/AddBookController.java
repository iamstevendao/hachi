package hachi.library.source.controller;

import hachi.library.source.function.FormatChecker;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import hachi.library.source.activity.AddBookActivity;
import hachi.library.source.activity.LogInActivity;

/**
 * Created by taohansamu on 4/23/2016.
 */
public class AddBookController implements Initializable {
    @FXML private  TextField txtBookcode;
    @FXML private TextField txtBookname;
    @FXML private  TextField txtAuthor;
    @FXML private TextField txtAmount;
    @FXML private TextField txtPrice;
    @FXML private Text actiontarget;
    public void handleSubmitButtonAction() throws Exception{
    if(txtBookcode.getText().equals("")||txtBookname.getText().equals("")||txtAuthor.getText().equals("")||txtAmount.getText().equals("")||txtPrice.getText().equals(""))
    {
        printActiontarget("Không được bỏ trống trường nào !");
    }
    else{
        AddBookActivity addBookActivity =new AddBookActivity();
        if(!FormatChecker.isNumericOnlyNumber(txtBookcode.getText()))
            printActiontarget("Mã sách phải là số !");
        else if(AddBookActivity.bookIsExist(txtBookcode.getText()))
            printActiontarget("Sách đã tồn tại !");
        else if(!FormatChecker.isNumericOnlyNumber(txtAmount.getText()))
            printActiontarget("Số lượng sách nhập không đúng !");
        else if(!FormatChecker.isNumericOnlyNumber(txtPrice.getText()))
            printActiontarget("Giá nhập không đúng !");
        else
        {

           addBookActivity.addBook(txtBookcode.getText(),txtBookname.getText(),txtPrice.getText(),txtAmount.getText(),txtAuthor.getText());
            printActiontarget("Đã thêm sách mới !");
        }
    }
    }
    public  void printActiontarget(String message)
    {
        actiontarget.setText(message);
        actiontarget.setFill(Color.RED);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try{
            ResultSet rs=LogInActivity.pd.query("SELECT MAX(book_id)+1 as tmp FROM book;");
            if(rs.next()){
                txtBookcode.setText(rs.getInt("tmp")+"");
            }
        }
        catch (Exception e){

        }
    }
}
 