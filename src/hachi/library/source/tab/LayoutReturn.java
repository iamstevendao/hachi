package hachi.library.source.tab;

import hachi.library.source.function.FormatChecker;
import hachi.library.source.function.ShowBookInfomation;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import hachi.library.source.activity.LogInActivity;

import javax.imageio.ImageIO;


/**
 * Created by Taohansamu on 04/05/2016.
 */
public class LayoutReturn implements Initializable {
    @FXML
    public Text txtOutday;
    @FXML
    Text txtPrice;
    @FXML
    TableView tblBookOwing;
    @FXML
    TextField txtUser;
    @FXML
    TextField txtFine;
    @FXML
    TextField txtBook;
    @FXML
    VBox vbxBook;
    @FXML
    Text txtUserName;
    @FXML
    Text txtMoney;
    @FXML
    Label lblTotalMoney;
    @FXML
    GridPane grdTop;
    @FXML
    GridPane grdBottomLeft;
    @FXML
    GridPane grdTopLeft;
    @FXML
    TableView tblBookReturn;
    @FXML
    Button bttnPay;
    @FXML
    ImageView imgAvatar;
    @FXML
    Text txtUserId;
    @FXML
    Text txtRespond;
    @FXML
    TextField txtBookRemove;
    private int totalPrice;
    private ObservableList<LayoutReturn.Book> bookOwing = FXCollections.observableArrayList();
    private ObservableList<LayoutReturn.Book> bookReturn = FXCollections.observableArrayList();
    private ResultSet rs;
    private int userId;
    private int userMoney;
    private String userName;
    private static boolean initialized = false;

    @Override
    @SuppressWarnings("unchecked")
    public void initialize(URL url, ResourceBundle rb) {
        txtFine.setMaxWidth(70);
        displayChildren(false);
        txtPrice.setText("0$");
        tblBookOwing.setRowFactory(tv -> {
            TableRow<LayoutReturn.Book> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    try {
                        Object object = tblBookOwing.getSelectionModel().getSelectedItem();
                        ShowBookInfomation.bookId = ((LayoutReturn.Book) object).getBookId();
                        ShowBookInfomation showBookInfomation = new ShowBookInfomation();
                        showBookInfomation.showBookInfo();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return row;
        });
    }
    @FXML
    protected void fine(KeyEvent e) throws Exception {
        if (e.getCode() == KeyCode.ENTER) {
            totalPrice -= Integer.parseInt(txtFine.getText());
            txtPrice.setText(totalPrice + "$");
        }
    }
    @FXML
    protected void signIn() throws Exception {

            boolean available;
            rs = LogInActivity.pd.query("SELECT user_id, user_name, user_money FROM user WHERE user_id="
                    + txtUser.getText() +
                    ";");
            available = rs.next();
            if (available) {
                displayChildren(true);
                userId = rs.getInt("user_id");
                userName = rs.getString("user_name");
                userMoney = rs.getInt("user_money");
                txtUserId.setText(Integer.toString(userId));
                txtUserName.setText(userName);
                txtMoney.setText(userMoney + "$");
                totalPrice = 0;
                imgAvatar.setFitWidth(70);
                imgAvatar.setFitHeight(70);
                try {
                    File file = new File("F://hachi/src/hachi/library/image/user_avatar/" + userId + ".jpg");
                    BufferedImage bufferedImage = ImageIO.read(file);
                    Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                    imgAvatar.setImage(image);
                } catch (Exception ex) {
                    try {
                        File file = new File("F://hachi/src/hachi/library/image/user.png");
                        BufferedImage bufferedImage = ImageIO.read(file);
                        Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                        imgAvatar.setImage(image);
                    } catch (Exception exx) {

                    }
                }
                

                    initializeBookLayout();

                
            } else {
                txtRespond.setText("Mã bạn đọc không tồn tại !");
                // hbxWelcome.setVisible(false);
                // vbxBook.setVisible(false);
            }


    }

    @FXML
    protected void signInButton() throws Exception {
        boolean available;
        rs = LogInActivity.pd.query("SELECT user_id, user_name, user_money FROM user WHERE user_id="
                + txtUser.getText() +
                ";");
        available = rs.next();
        if (available) {
            displayChildren(true);
            userId = rs.getInt("user_id");
            userName = rs.getString("user_name");
            userMoney = rs.getInt("user_money");
            txtUserId.setText(Integer.toString(userId));
            txtUserName.setText(userName);
            txtMoney.setText(userMoney + "$");
            totalPrice = 0;
            imgAvatar.setFitWidth(70);
            imgAvatar.setFitHeight(70);
            try {
                File file = new File("F://hachi/src/hachi/library/image/user_avatar/" + userId + ".jpg");
                BufferedImage bufferedImage = ImageIO.read(file);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                imgAvatar.setImage(image);
            } catch (Exception ex) {
                try {
                    File file = new File("F://hachi/src/hachi/library/image/user.png");
                    BufferedImage bufferedImage = ImageIO.read(file);
                    Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                    imgAvatar.setImage(image);
                } catch (Exception exx) {
                    exx.printStackTrace();
                }
            }
                initializeBookLayout();
        }
    }

    @SuppressWarnings("unchecked")
    private void initializeBookLayout() {
        bookOwing.clear();
        bookReturn.clear();
        if (!initialized) {
            TableColumn colBookId = new TableColumn("Mã Sách");
            TableColumn colBookName = new TableColumn("Sách");
            TableColumn colAuthorName = new TableColumn("Tác Giả");
            TableColumn colBookAmount = new TableColumn("Giá Sách");
            TableColumn colDateBorrow = new TableColumn("Ngày Mượn");
            TableColumn colOutOfDate = new TableColumn("Số ngày quá hạn");
            colBookId.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, Integer>("bookId"));
            colBookName.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, String>("bookName"));
            colAuthorName.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, String>("authorName"));
            colBookAmount.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, Integer>("bookPrice"));
            colDateBorrow.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, String>("borrowDate"));
            colOutOfDate.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, Integer>("outOfDate"));
            tblBookOwing.getColumns().addAll(colBookId, colBookName, colAuthorName, colBookAmount, colDateBorrow, colOutOfDate);
            tblBookOwing.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

            TableColumn colBookId2 = new TableColumn("Mã Sách");
            TableColumn colBookName2 = new TableColumn("Sách");
            TableColumn colAuthorName2 = new TableColumn("Tác Giả");
            TableColumn colBookAmount2 = new TableColumn("Giá Sách");
            TableColumn colDateBorrow2 = new TableColumn("Ngày Mượn");
            TableColumn colOutOfDate2 = new TableColumn("Số ngày quá hạn");
            colBookId2.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, Integer>("bookId"));
            colBookName2.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, String>("bookName"));
            colAuthorName2.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, String>("authorName"));
            colBookAmount2.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, Integer>("bookPrice"));
            colDateBorrow2.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, String>("borrowDate"));
            colOutOfDate2.setCellValueFactory(new PropertyValueFactory<LayoutReturn.Book, Integer>("outOfDate"));
            tblBookReturn.getColumns().addAll(colBookId2, colBookName2, colAuthorName2, colBookAmount2, colDateBorrow2, colOutOfDate2);
            tblBookReturn.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

            initialized = true;
        }
        try {
            String query = "SELECT book_id,book_name, author, book_price"
                    + " FROM book  NATURAL JOIN order_line"
                    + " WHERE order_id IN (SELECT order_id FROM user_order WHERE user_id="
                    + txtUser.getText() + ") AND line_return_date IS NULL;";
            System.out.println(query);
           // rs = LogInActivity.pd.query(query);

            rs = LogInActivity.pd.query("SELECT book_id, book_name, author, book_price, order_register_date, DATEDIFF(current_date,order_register_date) as out_of_date " +
                    "FROM order_line NATURAL JOIN user_order NATURAL JOIN book NATURAL JOIN lib.`order` WHERE user_id ='" + userId + "' AND line_return_date IS NULL");
            
            while (rs.next()) {
                int bookId = rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String authorName = rs.getString("author");
                int bookPrice = rs.getInt("book_price");
                String borrowDate = rs.getString("order_register_date");
                int outOfDate = rs.getInt("out_of_date");
              //  totalPrice += bookPrice - outOfDate * 3;
                bookOwing.add(new Book(bookId, bookName, authorName, bookPrice, borrowDate, outOfDate));
            }
            tblBookOwing.setItems(bookOwing);


        } catch (Exception e) {
            e.printStackTrace();
        }
        tblBookOwing.setRowFactory( tv -> {
            TableRow<LayoutReturn.Book> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                            try {
                                Object object = tblBookOwing.getSelectionModel().getSelectedItem();
                                int bookId = ((LayoutReturn.Book)object).getBookId();
                                ShowBookInfomation.bookId=bookId;
                                ShowBookInfomation showBookInfomation=new ShowBookInfomation();
                                showBookInfomation.showBookInfo();
                            } catch (Exception e) {

                            }

                        }
                if (event.getClickCount() == 1 && (! row.isEmpty()) ) {
                    try {
                        Object object = tblBookOwing.getSelectionModel().getSelectedItem();
                        int bookId = ((LayoutReturn.Book)object).getBookId();
                        txtBook.setText(""+bookId);
                    } catch (Exception e) {

                    }

                }
                    }
            );
            return row ;
        });
        tblBookReturn.setRowFactory( tv -> {
            TableRow<LayoutReturn.Book> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                            try {
                                Object object = tblBookReturn.getSelectionModel().getSelectedItem();
                                int bookId = ((LayoutReturn.Book)object).getBookId();
                                ShowBookInfomation.bookId=bookId;
                                ShowBookInfomation showBookInfomation=new ShowBookInfomation();
                                showBookInfomation.showBookInfo();
                            } catch (Exception e) {

                            }

                        }
                        if (event.getClickCount() == 1 && (! row.isEmpty()) ) {
                            try {
                                Object object = tblBookReturn.getSelectionModel().getSelectedItem();
                                int bookId = ((LayoutReturn.Book)object).getBookId();
                                txtBookRemove.setText(""+bookId);
                            } catch (Exception e) {

                            }

                        }
                    }
            );
            return row ;
        });
    }

    @FXML
    public void signOut() throws Exception {
        displayChildren(false);
        bookOwing.clear();
        txtPrice.setVisible(false);
        txtRespond.setText("");
        txtPrice.setText("0 $");
    }

    @FXML
    @SuppressWarnings("unchecked")
    public void addBook() throws Exception {
        boolean available = false;
        for (Book book : bookOwing){
            if(book.getBookId() == Integer.parseInt(txtBook.getText())) {
                bookReturn.add(book);
                bookOwing.remove(book);
                tblBookReturn.setItems(bookReturn);
                tblBookOwing.setItems(bookOwing);
                totalPrice += book.getBookPrice() - book.getOutOfDate()*3;
                txtPrice.setText(totalPrice + "$");
                available = true;
                break;
            }
        }
        if(!available){
            txtRespond.setText("Bạn đọc không mượn sách này");
        }
    }

    @FXML
    @SuppressWarnings("unchecked")
    public void removeBook() throws Exception {
        boolean available = false;
        for (Book book : bookReturn){
            if(book.getBookId() == Integer.parseInt(txtBookRemove.getText())) {
                bookOwing.add(book);
                bookReturn.remove(book);
                tblBookReturn.setItems(bookReturn);
                tblBookOwing.setItems(bookOwing);
                totalPrice -= book.getBookPrice();
                totalPrice +=  book.getOutOfDate()*3;
                txtPrice.setText(totalPrice + "$");
                available = true;
                break;
            }
        }
        if(!available){
            txtRespond.setText("Bạn đọc không mượn sách này");
        }
    }
    @FXML
    public void returnBook() throws Exception{
        Date current_date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
        String str_cdate = ft.format(current_date);
        int fine_one_day=0;
        int period=0;
        int totalProfit=0;
        int profit=0;
        int book_price = 0;
        int totalBookPrice=0;
        int total_outday_fine = 0;
        int totalOutDay = 0;
        int line_id=0;
        int order_id=0;
        rs = LogInActivity.pd.query("SELECT fine_one_day,period FROM regulations LIMIT 1;");
        if(rs.next()){
            fine_one_day = rs.getInt("fine_one_day");
            period = rs.getInt("period");
        }
        if(bookReturn.size()>0){
            int bookId;
            int i=0;
            String query;
            for(Book tmpBook:bookReturn){
                bookId=tmpBook.getBookId();
                book_price=tmpBook.getBookPrice();
                totalOutDay+=tmpBook.getOutOfDate();
                userMoney+=book_price;
                totalBookPrice+=book_price;
                query = "SELECT * FROM order_line NATURAL JOIN user_order WHERE user_id="
                        + userId
                        + " AND book_id="
                        + bookId
                        + " AND line_return_date IS NULL LIMIT 1;";
                rs = LogInActivity.pd.query(query);
                if(rs.next()){
                    order_id = rs.getInt("order_id");
                    line_id = rs.getInt("line_id");
                }
                profit = Integer.parseInt(txtFine.getText()) + tmpBook.getOutOfDate()*fine_one_day;
                userMoney-=profit;
                query = "UPDATE order_line SET line_return_date='"
                        + str_cdate + "',line_out_dates=" + tmpBook.getOutOfDate() + ",line_profit=" + profit
                        + " WHERE order_id=" + order_id + " AND line_id=" + line_id + ";";
                LogInActivity.pd.queryUpdate(query);
                totalProfit+=profit;

            }
            query = "UPDATE user SET user_money=" + userMoney + " WHERE user_id=" + userId + ";";
            System.out.println(query);
            LogInActivity.pd.queryUpdate(query);
            txtRespond.setText("Trả sách thành công!");
            txtMoney.setText(""+userMoney);
            bookReturn.clear();
        }
        


    }


    public void signInByEnter(KeyEvent event) throws Exception{
        if(event.getCode()==KeyCode.ENTER){
            signIn();
        }
    }
    @FXML
    public void removeBookByEnter(KeyEvent event) throws Exception{
        if(event.getCode()==KeyCode.ENTER){
            removeBook();
        }
    }
    public void addBookByEnter(KeyEvent event) throws Exception{
        if(event.getCode()==KeyCode.ENTER){
            addBook();
        }
    }

    public void refresh(ActionEvent actionEvent) {
        initializeBookLayout();
        txtMoney.setText(""+userMoney);
    }

    public class Book {
        private final SimpleIntegerProperty bookId;
        private final SimpleStringProperty bookName;
        private final SimpleStringProperty authorName;
        private final SimpleIntegerProperty bookPrice;
        private final SimpleStringProperty borrowDate;
        private final SimpleIntegerProperty outOfDate;

        private Book(int bookId, String bookName, String authorName, int bookPrice, String borrowDate, int outOfDate) {
            this.bookId = new SimpleIntegerProperty(bookId);
            this.bookName = new SimpleStringProperty(bookName);
            this.authorName = new SimpleStringProperty(authorName);
            this.bookPrice = new SimpleIntegerProperty(bookPrice);
            this.borrowDate = new SimpleStringProperty(borrowDate);
            this.outOfDate = new SimpleIntegerProperty(outOfDate);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return (this.bookName.toString().equals(((LayoutReturn.Book) obj).bookName.toString())
                    && this.authorName.toString().equals(((LayoutReturn.Book) obj).authorName.toString())
                    && this.bookPrice.intValue() == ((LayoutReturn.Book) obj).bookPrice.intValue());
        }
        @Override
        public int hashCode() {
            return 7 + 5*bookPrice.intValue(); // 5 and 7 are random prime numbers
        }

        public int getBookId() {
            return bookId.get();
        }

        public SimpleIntegerProperty bookIdProperty() {
            return bookId;
        }

        public String getBookName() {
            return bookName.get();
        }

        public SimpleStringProperty bookNameProperty() {
            return bookName;
        }

        public int getBookPrice() {
            return bookPrice.get();
        }

        public SimpleIntegerProperty bookPriceProperty() {
            return bookPrice;
        }

        public String getAuthorName() {
            return authorName.get();
        }

        public SimpleStringProperty authorNameProperty() {
            return authorName;
        }

        public String getBorrowDate() {
            return borrowDate.get();
        }

        public SimpleStringProperty borrowDateProperty() {
            return borrowDate;
        }

        public void setBorrowDate(String borrowDate) {
            this.borrowDate.set(borrowDate);
        }

        public int getOutOfDate() {
            return outOfDate.get();
        }

        public SimpleIntegerProperty outOfDateProperty() {
            return outOfDate;
        }

        public void setOutOfDate(int outOfDate) {
            this.outOfDate.set(outOfDate);
        }
    }

    public void displayChildren(boolean bool) {
        grdBottomLeft.setVisible(bool);
        grdTop.setVisible(bool);
        lblTotalMoney.setVisible(bool);
        vbxBook.setVisible(bool);
        txtPrice.setVisible(bool);
        bttnPay.setVisible(bool);
        grdTopLeft.setVisible(!bool);
    }

}
