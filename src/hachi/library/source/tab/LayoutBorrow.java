package hachi.library.source.tab;

import hachi.library.source.activity.ConfirmBorrowActivity;
import hachi.library.source.activity.LogInActivity;
import hachi.library.source.function.ShowBookInfomation;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;


/**
 * Created by Fukie on 12/04/2016.
 */
public class LayoutBorrow implements Initializable{
    @FXML
    Text txtPrice;
    @FXML
    TableView tblBook;
    @FXML
    TextField txtUser;
    @FXML
    Text txtUserId;
    @FXML
    TextField txtBook;
    @FXML
    ImageView imgAvatar;
    @FXML
    Button bttnDeleteBook;
    @FXML
    Text txtUserName;
    @FXML
     Text txtMoney;
    @FXML
    Button bttnPay;
    @FXML
    Label lblTotalMoney;
    @FXML
    Text txtBookAvailable;
    @FXML
    GridPane grdTop;
    @FXML
    GridPane grdBottomLeft;
    @FXML
    GridPane grdTopLeft;
    public static boolean payed;
    private int totalPrice;
    private TableColumn colBookName = new TableColumn("Sách");
    private TableColumn colAuthorName = new TableColumn("Tác Giả");
    private TableColumn colBookAmount = new TableColumn("Giá");
    private final ObservableList<LayoutBorrow.Book> books = FXCollections.observableArrayList();
    private ResultSet rs;
    private int userMoney;
    private int userId;
    private String userName;
    private static boolean initialized = false;
    @Override
    @SuppressWarnings("unchecked")
    public void initialize(URL url, ResourceBundle rb) {
        grdTop.setPadding(new Insets(0, 0, 10, 40));
        displayChildren(false);
        tblBook.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if(tblBook.getSelectionModel().getSelectedItem() != null)
            {
                bttnDeleteBook.setDisable(false);
            } else {
                bttnDeleteBook.setDisable(true);
            }
        });
        txtPrice.setText("0$");
    }

    @FXML
    protected void signIn() throws Exception {
        boolean available;
        rs = LogInActivity.pd.query("SELECT user_id, user_name, user_money FROM user WHERE user_id='"
                + txtUser.getText() + "';");
        if (rs.next()) {
            available = true;
        } else {
            rs = LogInActivity.pd.query("SELECT user_id, user_name, user_money FROM user WHERE user_name='"
                    + txtUser.getText()
                    + "';");
            available = rs.next();
        }
        if (available) {
            payed=false;
            displayChildren(true);
            userId = rs.getInt("user_id");
            userName = rs.getString("user_name");
            userMoney = rs.getInt("user_money");
            txtUserId.setText(Integer.toString(userId));
            txtUserName.setText(userName);
            txtMoney.setText(userMoney + "$");
            totalPrice = 0;
            imgAvatar.setFitWidth(70);
            imgAvatar.setFitHeight(70);
            try {
                File file = new File("F://hachi/src/hachi/library/image/user_avatar/" + userId + ".jpg");
                BufferedImage bufferedImage = ImageIO.read(file);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                imgAvatar.setImage(image);
            } catch (Exception e) {
                try {
                    File file = new File("F://hachi/src/hachi/library/image/user.png");
                    BufferedImage bufferedImage = ImageIO.read(file);
                    Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                    imgAvatar.setImage(image);
                } catch (Exception ex){

                }
            }
            if(!initialized)
                initializeBookLayout();
        } else {
        }
    }

    @FXML
    protected void signOut() throws Exception {
        displayChildren(false);
        txtUser.setText("");
        books.clear();
        txtPrice.setVisible(false);
    }
    @FXML
    @SuppressWarnings("unchecked")
    protected void addBook() throws Exception {
        txtBookAvailable.setText("");
        displayChildren(true);
        boolean available;
        txtPrice.setVisible(true);
        ResultSet rs = LogInActivity.pd.query("SELECT book_id, book_name, author, book_price, book_storing FROM book WHERE book_id='"
                + txtBook.getText()
                + "'");
        if (rs.next()) {
            available = true;
        } else {
            rs = LogInActivity.pd.query("SELECT book.book_id, book_name, author, book_price, book_storing FROM book  WHERE book_name LIKE '%"
                    + txtBook.getText()
                    + "%'");
            available = rs.next();
        }
        if (available) {
            int bookId = rs.getInt("book_id");
            String bookName = rs.getString("book_name");
            String authorName = rs.getString("author");
            int bookPrice = rs.getInt("book_price");
            int bookStoring = rs.getInt("book_storing");
            if(bookStoring == 0) {
                txtBookAvailable.setText("Sách không có sẵn ");
            } else {
                rs = LogInActivity.pd.query("SELECT book_id FROM order_line NATURAL JOIN user_order WHERE user_id ='" + userId + "' AND line_return_date IS NULL");
                boolean isOwing = false;
                while (rs.next()) {
                    if (bookId == rs.getInt("book_id")) {
                        isOwing = true;
                        txtBookAvailable.setText("Sách vẫn đang nợ");
                        break;
                    }
                }
                if (!books.contains(new LayoutBorrow.Book(bookId, bookName, authorName, bookPrice)) && !isOwing) {
                    books.add(new LayoutBorrow.Book(bookId, bookName, authorName, bookPrice));
                    totalPrice += bookPrice;
                    txtPrice.setText(totalPrice + "$");
                    tblBook.setItems(books);
                    tblBook.prefHeightProperty().bind(tblBook.fixedCellSizeProperty().multiply(Bindings.size(tblBook.getItems()).add(1.01)));
                }
            }
        }
    }

    @FXML
    @SuppressWarnings("unchecked")
    protected void deleteBook() {
        Object object = tblBook.getSelectionModel().getSelectedItem();
        books.remove(tblBook.getSelectionModel().getSelectedIndex());
        String priceNumber = txtPrice.getText().substring(0, txtPrice.getText().length() - 1);
        int k = Integer.parseInt(priceNumber) - ((Book)object).bookPrice.intValue();
        txtPrice.setText(k + "$");
        tblBook.setItems(books);
    }

    @FXML
    protected void payBook() throws Exception{
        if(books.size() > 0) {
            String[] bks = new String[books.size()];
            int[] bksPrice = new int[books.size()];
            int[] bksId = new int[books.size()];
            int i = 0;
            for (Book tmpBook : books) {
                bksId[i] = tmpBook.getBookId();
                bks[i] = tmpBook.getBookName();
                bksPrice[i++] = tmpBook.getBookPrice();
            }
            ConfirmBorrowActivity confirmBorrowActivity = new ConfirmBorrowActivity(userId, userName, userMoney, bksId, bks, bksPrice);
            confirmBorrowActivity.start(new Stage());

        }
    }

    @SuppressWarnings("unchecked")
    private  void initializeBookLayout() {
        initialized = true;
        tblBook.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tblBook.setFixedCellSize(25);
        tblBook.prefHeightProperty().bind(tblBook.fixedCellSizeProperty().multiply(Bindings.size(tblBook.getItems()).add(1.01)));
        tblBook.setPlaceholder(new Label(""));
        colBookName.setCellValueFactory(new PropertyValueFactory<LayoutBook.Book, String>("bookName"));
        colAuthorName.setCellValueFactory(new PropertyValueFactory<LayoutBook.Book, String>("authorName"));
        colBookAmount.setCellValueFactory(new PropertyValueFactory<LayoutBook.Book, Integer>("bookPrice"));
        tblBook.getColumns().addAll(colBookName, colAuthorName, colBookAmount);
        tblBook.setRowFactory( tv -> {
            TableRow<LayoutBorrow.Book> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                            try {
                                Object object = tblBook.getSelectionModel().getSelectedItem();
                                int bookId = ((LayoutBorrow.Book)object).getBookId();
                                ShowBookInfomation.bookId=bookId;
                                ShowBookInfomation showBookInfomation=new ShowBookInfomation();
                                showBookInfomation.showBookInfo();
                            } catch (Exception e) {

                            }
                        }
                if (event.getClickCount() == 1 && (! row.isEmpty()) ) {
                    try {
                        Object object = tblBook.getSelectionModel().getSelectedItem();
                        int bookId = ((LayoutBorrow.Book)object).getBookId();
                        txtBook.setText(""+bookId);
                    } catch (Exception e) {

                    }
                }
                    }
            );
            return row ;
        });
    }



    public void enterSign(KeyEvent event) throws Exception {
        if(event.getCode()== KeyCode.ENTER){
            signIn();
        }
    }

    public void enterAddBook(KeyEvent event) throws Exception {
        if(event.getCode()== KeyCode.ENTER){
            addBook();
        }
    }

    public void signInByEnter(KeyEvent event)throws Exception {
        if(event.getCode()==KeyCode.ENTER){
            signIn();
        }
    }


    public class Book {
        private final SimpleIntegerProperty bookId;
        private final SimpleStringProperty bookName;
        private final SimpleStringProperty authorName;
        private final SimpleIntegerProperty bookPrice;

        private Book(int bookId, String bookName, String authorName, int bookPrice) {
            this.bookId = new SimpleIntegerProperty(bookId);
            this.bookName = new SimpleStringProperty(bookName);
            this.authorName = new SimpleStringProperty(authorName);
            this.bookPrice = new SimpleIntegerProperty(bookPrice);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return (this.bookName.toString().equals(((Book) obj).bookName.toString())
                    && this.authorName.toString().equals(((Book) obj).authorName.toString())
                    && this.bookPrice.intValue() == ((Book) obj).bookPrice.intValue());
        }
        @Override
        public int hashCode() {
            return 7 + 5*bookPrice.intValue(); // 5 and 7 are random prime numbers
        }

        public int getBookId() {
            return bookId.get();
        }

        public SimpleIntegerProperty bookIdProperty() {
            return bookId;
        }

        public String getBookName() {
            return bookName.get();
        }

        public SimpleStringProperty bookNameProperty() {
            return bookName;
        }

        public int getBookPrice() {
            return bookPrice.get();
        }

        public SimpleIntegerProperty bookPriceProperty() {
            return bookPrice;
        }

        public String getAuthorName() {
            return authorName.get();
        }

        public SimpleStringProperty authorNameProperty() {
            return authorName;
        }
    }

    private void displayChildren(boolean bool) {
        grdBottomLeft.setVisible(bool);
        grdTop.setVisible(bool);
        lblTotalMoney.setVisible(bool);
        tblBook.setVisible(bool);
        txtPrice.setVisible(bool);
        bttnPay.setVisible(bool);
        grdTopLeft.setVisible(!bool);
    }
}
