package hachi.library.source.controller;

import hachi.library.source.activity.AddUserActivity;
import hachi.library.source.function.FormatChecker;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static com.sun.deploy.cache.Cache.copyFile;
import static com.sun.deploy.config.Config.getCacheDirectory;

/**
 * Created by Fukie on 01/05/2016.
 */
public class AddUserController implements Initializable {
    @FXML
    TextField txtUserName;
    @FXML
    TextField txtUserPhone;
    @FXML
    TextField txtUserEmail;
    @FXML
    TextField txtUserAddress;
    @FXML
    TextField txtUserMoney;
    @FXML
    ImageView imgAvatar;
    @FXML
    Text actionTarget;
    @FXML
    Text txtTargetName;
    @FXML
    ImageView imgName;
    @FXML
    Text txtTargetPhone;
    @FXML
    ImageView imgPhone;
    @FXML
    Text txtTargetEmail;
    @FXML
    ImageView imgEmail;
    @FXML
    Text txtTargetAddress;
    @FXML
    ImageView imgAddress;
    @FXML
    Text txtTargetMoney;
    @FXML
    ImageView imgMoney;
    @FXML
    Button bttnSubmit;
    private boolean[] isValid = new boolean[5];
    File file;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtUserName.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (!newValue) {
                        if (txtUserName.getText().length() == 0) {
                            imgName.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetName.setText("Vui lòng nhập tên bạn đọc!");
                            isValid[0] = false;
                        } else if (FormatChecker.isContainsNumber(txtUserName.getText())) {
                            imgName.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetName.setText("Tên không đúng!");
                            isValid[0] = false;
                        } else {
                            imgName.setImage(new Image("/hachi/library/image/valid.png"));
                            txtTargetName.setText("");
                            isValid[0] = true;
                        }
                        checkValidFields();
                    }
                });

        txtUserPhone.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (!newValue) {
                        if (txtUserPhone.getText().length() == 0) {
                            imgPhone.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetPhone.setText("Vui lòng nhập số điện thoại!");
                            isValid[1] = false;
                        } else if (!FormatChecker.isNumericOnlyNumber(txtUserPhone.getText())) {
                            imgPhone.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetPhone.setText("Số điện thoại không đúng!");
                            isValid[1] = false;
                        } else {
                            imgPhone.setImage(new Image("/hachi/library/image/valid.png"));
                            txtTargetPhone.setText("");
                            isValid[1] = true;
                        }
                        checkValidFields();
                    }
                });

        txtUserEmail.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (!newValue) {
                        if (txtUserEmail.getText().length() == 0) {
                            imgEmail.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetEmail.setText("Vui lòng nhập email!");
                            isValid[2] = false;
                        } else if (txtUserEmail.getText().indexOf('@') < 0) {
                            imgEmail.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetEmail.setText("Email không đúng!");
                            isValid[2] = false;
                        } else {
                            imgEmail.setImage(new Image("/hachi/library/image/valid.png"));
                            txtTargetEmail.setText("");
                            isValid[2] = true;
                        }
                        checkValidFields();
                    }
                });

        txtUserAddress.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (!newValue) {
                        if (txtUserAddress.getText().length() == 0) {
                            imgAddress.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetAddress.setText("Vui lòng nhập địa chỉ!");
                            isValid[3] = false;
                        } else {
                            imgAddress.setImage(new Image("/hachi/library/image/valid.png"));
                            txtTargetAddress.setText("");
                            isValid[3] = true;
                        }
                        checkValidFields();
                    }
                });

        txtUserMoney.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (!newValue) {
                        if (txtUserMoney.getText().length() == 0) {
                            imgMoney.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetMoney.setText("Vui lòng nhập Tiền nạp lần đầu");
                            isValid[4] = false;
                        } else if (!FormatChecker.isNumericOnlyNumber(txtUserMoney.getText())) {
                            imgMoney.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetMoney.setText("Số Tiền nhập không đúng");
                            isValid[4] = false;
                        } else {
                            imgMoney.setImage(new Image("/hachi/library/image/valid.png"));
                            txtTargetMoney.setText("");
                            isValid[4] = true;
                        }
                        checkValidFields();
                    }
                });
    }

    @FXML
    protected void changeAvatar() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
        File file = fileChooser.showOpenDialog(null);
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            this.file = file;
            // File fd = new File("../../image/user/" + file.getName() + ".mp3");
            imgAvatar.setImage(image);
            imgAvatar.setFitWidth(100);
            imgAvatar.setFitHeight(100);
        } catch (IOException ex) {
            System.out.println("hello");
        }
    }

    private void checkValidFields() {
        boolean check = true;
        for(boolean i : isValid) {
            if(!i)
                check = false;
        }
        if(check) {
            bttnSubmit.setDisable(false);
        } else {
            bttnSubmit.setDisable(true);
        }
    }

    @FXML
    protected void submit() throws Exception{
        int count = AddUserActivity.getInstance().addUser(txtUserName.getText(), txtUserPhone.getText(), txtUserEmail.getText(), txtUserAddress.getText(), Integer.parseInt(txtUserMoney.getText()));
        String fileName = file.getName();
        String[] fileName2 = fileName.split("\\.");
        String fileExtension = fileName2[1];
        File f = new File("F://hachi/src/hachi/library/image/user_avatar/" + count + "." + fileExtension);
        copyFile(this.file, f);
        Stage stage = (Stage) txtUserName.getScene().getWindow();
        stage.close();
    }
}
