package hachi.library.source.tab;

import hachi.library.source.activity.AddBookActivity;
import hachi.library.source.activity.BookActivity;
import hachi.library.source.activity.LogInActivity;
import hachi.library.source.function.ShowBookInfomation;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;
/**
 * Created by taohansamu on 5/8/2016.
 */
public class LayoutBookRank implements Initializable  {
    @FXML
    private TableView tblBook;
    @FXML
    private TextField txtSearchBook;
    ObservableList<LayoutBookRank.Book> books;
    ObservableList<String> showFormatOptions =
            FXCollections.observableArrayList(
                    "10",
                    "20",
                    "30",
                    "40",
                    "50",
                    "100"
            );
    @FXML
    private ComboBox cbxShowFormat;

    ObservableList<String> kindOptions =
            FXCollections.observableArrayList(
                    "Week",
                    "Month",
                    "Year"
            );

    @FXML
    private ComboBox cbxTopKind;
    private static int format=10;
    private static int kind=7;
    private static boolean initialized=false;
    @Override
    @SuppressWarnings("unchecked")
    public void initialize(URL location, ResourceBundle resources){
        cbxShowFormat.setItems(showFormatOptions);
        cbxShowFormat.setValue("10");
        cbxTopKind.setItems(kindOptions);
        cbxTopKind.setValue("Week");
        initializeBookRank();
    }
    public void initializeBookRank(){
        if(!initialized){

            TableColumn colBookRank=new TableColumn("Số Lượng Mượn");
            TableColumn colBookId=new TableColumn("Mã Sách");
            TableColumn colBookName = new TableColumn("Sách");
            TableColumn colAuthorName = new TableColumn("Tác Giả");
            colBookRank.setCellValueFactory(new PropertyValueFactory<LayoutBookRank.Book,Integer>("bookRank"));
            colBookId.setCellValueFactory(new PropertyValueFactory<LayoutBookRank.Book, Integer>("bookId"));
            colBookName.setCellValueFactory(new PropertyValueFactory<LayoutBookRank.Book, String>("bookName"));
            colAuthorName.setCellValueFactory(new PropertyValueFactory<LayoutBookRank.Book, String>("authorName"));
            tblBook.getColumns().addAll(colBookId,colBookName, colAuthorName,colBookRank);
            tblBook.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            books = FXCollections.observableArrayList();
            initialized=true;
        }
        else{
            books.clear();
        }
        try {

            String query="SELECT count(order_id) as book_rank,book_id,book_name, author"
                    +" FROM book NATURAL JOIN order_line"
                    +" WHERE line_return_date IS NULL OR line_return_date >= current_date-"+kind
                    +" GROUP BY book_id "
                    +" ORDER BY book_rank DESC"
                    +" LIMIT "+format+";";
            System.out.println(query);
            ResultSet rs = LogInActivity.pd.query(query);
            while (rs.next()) {
                int bookRank=rs.getInt("book_rank");
                int bookId=rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String authorName = rs.getString("author");
                books.add(new LayoutBookRank.Book(bookRank,bookId,bookName, authorName));
            }
            tblBook.setItems(books);
        } catch (Exception e) {

        }
        tblBook.setRowFactory( tv -> {
            TableRow<LayoutBookRank.Book> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    try {
                        Object object = tblBook.getSelectionModel().getSelectedItem();
                        int bookId = ((LayoutBookRank.Book)object).getBookId();
                        ShowBookInfomation.bookId=bookId;
                        ShowBookInfomation showBookInfomation=new ShowBookInfomation();
                        showBookInfomation.showBookInfo();
                    } catch (Exception e) {

                    }
                }
            });
            return row ;
        });
    }

    @FXML
    protected void addBook() throws Exception {
        AddBookActivity addBookActivity = new AddBookActivity();
        addBookActivity.start(new Stage());
    }

    @FXML
    protected void changeBook() {

    }

    @FXML
    protected void deleteBook() {

    }

    @FXML
    protected void analyzeBook() {

    }

    @FXML
    @SuppressWarnings("unchecked")
    protected void updateResultTable() throws Exception {
        books.clear();
        if(txtSearchBook.getText().length() > 0) {
            String query="SELECT count(order_id) as book_rank,book_id,book_name, author "
                        +"FROM book NATURAL JOIN order_line"
                        +" WHERE (line_return_date IS NULL OR line_return_date >= current_date-"+kind+" ) AND (book_id LIKE '"+txtSearchBook.getText()
                        + "%' OR book_name LIKE '%"
                        + txtSearchBook.getText()
                        + "%' OR author LIKE '%"
                        + txtSearchBook.getText()
                        + "%') GROUP BY book_id ORDER BY book_rank DESC"
                        +" LIMIT "+format+";";
            System.out.println(query);
            ResultSet rs = LogInActivity.pd.query(query);
            while (rs.next()) {
                int bookRank=rs.getInt("book_rank");
                int bookId=rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String authorName = rs.getString("author");
                books.add(new LayoutBookRank.Book(bookRank,bookId,bookName, authorName));
                tblBook.prefHeightProperty().bind(tblBook.fixedCellSizeProperty().multiply(Bindings.size(tblBook.getItems()).add(1.01)));
            }
        } else {
            String query="SELECT count(order_id) as book_rank,book_id,book_name, author "
                    +" FROM book  NATURAL JOIN order_line"
                    +" WHERE line_return_date IS NULL OR line_return_date >= current_date-"+kind
                    +" GROUP BY book_id"
                    +" ORDER BY book_rank DESC"
                    +" LIMIT "+format;
            ResultSet rs = LogInActivity.pd.query(query);
            while (rs.next()) {
                int bookRank=rs.getInt("book_rank");
                int bookId=rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String authorName = rs.getString("author");
                books.add(new LayoutBookRank.Book(bookRank,bookId,bookName, authorName));
            }
        }
        tblBook.setItems(books);
    }

    public void topKind(Event event) throws Exception {

        String tmp=cbxTopKind.getSelectionModel().getSelectedItem().toString();
        System.out.println(tmp);
        switch (tmp){
            case "Week":kind=7;break;
            case "Month":kind=30;break;
            case "Year" : kind=365;break;
            default:kind=7;
        }
       // kind=Integer.parseInt(tmp);
        initializeBookRank();
    }

    public void showFormat(Event event) throws Exception {
        String tmp=cbxShowFormat.getSelectionModel().getSelectedItem().toString();
        System.out.println(tmp);
        format=Integer.parseInt(tmp);
        initializeBookRank();
    }

    public class Book {
        private final SimpleIntegerProperty bookRank;
        private final SimpleIntegerProperty bookId ;
        private final SimpleStringProperty bookName;
        private final SimpleStringProperty authorName;

        private Book(int bookRank,int bookId, String bookName, String authorName) {
            this.bookRank = new SimpleIntegerProperty(bookRank);
            this.bookName = new SimpleStringProperty(bookName);
            this.authorName = new SimpleStringProperty(authorName);
            this.bookId = new SimpleIntegerProperty(bookId);
        }
        public int getBookRank()
        {
            return bookRank.get();
        }
        public SimpleIntegerProperty bookRankProperty() {
            return bookRank;
        }
        public int getBookId()
        {
            return bookId.get();
        }
        public SimpleIntegerProperty bookIdProperty() {
            return bookId;
        }
        public String getBookName() {
            return bookName.get();
        }

        public SimpleStringProperty bookNameProperty() {
            return bookName;
        }

        public String getAuthorName() {
            return authorName.get();
        }

        public SimpleStringProperty authorNameProperty() {
            return authorName;
        }
    }
}
