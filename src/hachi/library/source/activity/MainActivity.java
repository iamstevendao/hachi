package hachi.library.source.activity;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Fukie on 11/04/2016.
 */
public class MainActivity extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Quan li thu vien");
        stage.setScene(createScene(loadMainPane()));
        stage.show();
    }

    private Parent loadMainPane() throws IOException {
        return FXMLLoader.load(getClass().getResource("../../layout/main_actitvity.fxml"));
    }

    private Scene createScene(Parent root) {
        Scene scene = new Scene(root, 1000, 700);
        scene.getStylesheets().setAll(getClass().getResource("../../css/layout.css").toExternalForm());

        return scene;
    }
}
