package hachi.library.source.activity;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * Created by taohansamu on 4/23/2016.
 */

public class UserStatisActivity extends Application{
    public void start(Stage stage) throws Exception
    {
        Parent root= FXMLLoader.load(getClass().getResource("../../layout/user_statistic.fxml"));
        Scene scene= new Scene(root,400,300);
        stage.setTitle("Thống kê");
        stage.setScene(scene);
        stage.show();
    }
}
