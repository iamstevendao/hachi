package hachi.library.source.activity;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Fukie on 01/05/2016.
 */
public class AddUserActivity extends Application {
    private static AddUserActivity addUserActivity;
    public void start(Stage stage) throws Exception {
        addUserActivity = this;
        Parent root = FXMLLoader.load(getClass().getResource("../../layout/add_user.fxml"));
        Scene scene = new Scene(root, 500, 400);
        scene.getStylesheets().setAll(getClass().getResource("../../css/layout.css").toExternalForm());
        stage.setTitle("Them Ban doc");
        stage.setScene(scene);
        stage.show();
    }

    public int addUser(String name, String phone, String email, String address, int money) throws Exception{
        ResultSet rs = LogInActivity.pd.query("SELECT MAX(user_id) FROM `user`;");
        int count;
        if (rs.next()) {
            count = rs.getInt("MAX(user_id)") + 1;
        } else count = 1;
        String today = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        LogInActivity.pd.queryUpdate("INSERT INTO user VALUES(" + count + ",'" + name + "'," + 1 + ",'" + address + "','" + phone + "','" + money + "','" + email + "','" + today + "')");
        return count;
    }

    public static AddUserActivity getInstance() {
        return addUserActivity;
    }
}
