package hachi.library.source.tab;

import hachi.library.source.activity.LogInActivity;
import hachi.library.source.activity.UserActivity;
import hachi.library.source.function.FormatChecker;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import static com.sun.deploy.cache.Cache.copyFile;

/**
 * Created by Fukie on 09/05/2016.
 */
public class UserInformation implements Initializable {
    @FXML
    Text txtUserId;
    @FXML
    Text txtUserDate;
    @FXML
    TextField txtUserName;
    @FXML
    TextField txtUserPhone;
    @FXML
    TextField txtUserEmail;
    @FXML
    TextField txtUserAddress;
    @FXML
    ImageView imgAvatar;
    @FXML
    Text actionTarget;
    @FXML
    Text txtTargetName;
    @FXML
    ImageView imgName;
    @FXML
    Text txtTargetPhone;
    @FXML
    ImageView imgPhone;
    @FXML
    Text txtTargetEmail;
    @FXML
    ImageView imgEmail;
    @FXML
    Text txtTargetAddress;
    @FXML
    ImageView imgAddress;
    @FXML
    Button bttnSubmit;
    private boolean[] isValid = new boolean[5];
    File file;
    private boolean isChangedAvatar;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        int userId = UserActivity.userId;
        for (int i = 0; i < isValid.length; i++) {
            isValid[i] = true;
        }
        checkValidFields();
        txtUserId.setText(Integer.toString(userId));
        txtUserDate.requestFocus();
        try {
            ResultSet rs = LogInActivity.pd.query("SELECT * FROM user where user_id='" + userId + "'");
            if (rs.next()) {
                txtUserDate.setText(rs.getString("user_register_date"));
                txtUserAddress.setText(rs.getString("user_address"));
                txtUserEmail.setText(rs.getString("user_email"));
                txtUserName.setText(rs.getString("user_name"));
                txtUserPhone.setText(rs.getString("user_phone"));
            }
        } catch (Exception e) {

        }
        txtUserName.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (!newValue) {
                        if (txtUserName.getText() == null) {
                            imgName.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetName.setText("Vui lòng nhập tên bạn đọc!");
                            isValid[0] = false;
                        } else if (FormatChecker.isContainsNumber(txtUserName.getText())) {
                            imgName.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetName.setText("Tên không đúng!");
                            isValid[0] = false;
                        } else {
                            imgName.setImage(new Image("/hachi/library/image/valid.png"));
                            txtTargetName.setText("");
                            isValid[0] = true;
                        }
                        checkValidFields();
                    }
                });

        txtUserPhone.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (!newValue) {
                        if (txtUserPhone.getText() == null) {
                            imgPhone.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetPhone.setText("Vui lòng nhập số điện thoại!");
                            isValid[1] = false;
                        } else if (!FormatChecker.isNumericOnlyNumber(txtUserPhone.getText())) {
                            imgPhone.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetPhone.setText("Số điện thoại không đúng!");
                            isValid[1] = false;
                        } else {
                            imgPhone.setImage(new Image("/hachi/library/image/valid.png"));
                            txtTargetPhone.setText("");
                            isValid[1] = true;
                        }
                        checkValidFields();
                    }
                });

        txtUserEmail.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (!newValue) {
                        if (txtUserEmail.getText() == null) {
                            imgEmail.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetEmail.setText("Vui lòng nhập email!");
                            isValid[2] = false;
                        } else if (txtUserEmail.getText().indexOf('@') < 0) {
                            imgEmail.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetEmail.setText("Email không đúng!");
                            isValid[2] = false;
                        } else {
                            imgEmail.setImage(new Image("/hachi/library/image/valid.png"));
                            txtTargetEmail.setText("");
                            isValid[2] = true;
                        }
                        checkValidFields();
                    }
                });

        txtUserAddress.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (!newValue) {
                        if (txtUserAddress.getText() == null) {
                            imgAddress.setImage(new Image("/hachi/library/image/invalid.png"));
                            txtTargetAddress.setText("Vui lòng nhập địa chỉ!");
                            isValid[3] = false;
                        } else {
                            imgAddress.setImage(new Image("/hachi/library/image/valid.png"));
                            txtTargetAddress.setText("");
                            isValid[3] = true;
                        }
                        checkValidFields();
                    }
                });

        try {
            //System.getProperty("user.dir");
            File file = new File("F://hachi/src/hachi/library/image/user_avatar/" + userId + ".jpg");
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            imgAvatar.setImage(image);
            imgAvatar.setFitWidth(150);
            imgAvatar.setFitHeight(150);
        } catch (Exception e) {

        }
    }

    @FXML
    protected void changeAvatar() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        //  FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG);
        File file = fileChooser.showOpenDialog(null);
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            this.file = file;
            isChangedAvatar = true;
            imgAvatar.setImage(image);
            imgAvatar.setFitWidth(150);
            imgAvatar.setFitHeight(150);
        } catch (IOException ex) {
            System.out.println("hello");
        }
    }

    private void checkValidFields() {
        boolean check = true;
        for (boolean i : isValid) {
            if (!i)
                check = false;
        }
        if (check) {
            bttnSubmit.setDisable(false);
        } else {
            bttnSubmit.setDisable(true);
        }
    }

    @FXML
    protected void submit() throws Exception {
        //int count = AddUserActivity.getInstance().addUser(txtUserName.getText(), txtUserPhone.getText(), txtUserEmail.getText(), txtUserAddress.getText(), Integer.parseInt(txtUserMoney.getText()));
        LogInActivity.pd.queryUpdate("UPDATE user SET user_name='" + txtUserName.getText()
                + "', user_email='" + txtUserEmail.getText()
                + "',user_phone='" + txtUserPhone.getText()
                + "', user_address='" + txtUserAddress.getText()
                + "' WHERE user_id=" + txtUserId.getText());
        if (isChangedAvatar) {
            File f = new File("F://hachi/src/hachi/library/image/user_avatar/" + txtUserId.getText() + ".jpg");
            copyFile(this.file, f);
        }
        Stage stage = (Stage) txtUserName.getScene().getWindow();
        stage.close();
    }
}
